#pragma once
#include "action_simple.h"
#include "world_state_simple.h"
#include "action_planner_simple.h"
#include "a_star_plan_simple.h"


action_planner_simple create_fps_action_planner()
{
	action_planner_simple action_planner;
	action_planner.add_action(action_simple("scout",
		{ {"armedwithgun", true} },
		{ {"enemyvisible", true} }));

	action_planner.add_action(action_simple("approach",
		{ {"enemyvisible", true} },
		{ {"nearenemy", true} }));

	action_planner.add_action(action_simple("aim",
		{ {"enemyvisible", true}, {"weaponloaded", true} },
		{ {"enemylinedup", true} }));

	action_planner.add_action(action_simple("shoot",
		{ {"enemylinedup", true} },
		{ {"enemyalive", false} }));

	action_planner.add_action(action_simple("load",
		{ {"armedwithgun", true} },
		{ {"weaponloaded", true} }));

	action_planner.add_action(action_simple("detonatebomb", 5.0f,
		{ {"armedwithbomb", true}, {"nearenemy", true} },
		{ {"alive", false}, {"enemyalive", false} }));

	action_planner.add_action(action_simple("flee",
		{ {"enemyvisible", true} },
		{ {"nearenemy", false} }));

	return action_planner;
}

void goap_test()
{
	// https://gamedevelopment.tutsplus.com/tutorials/goal-oriented-action-planning-for-a-smarter-ai--cms-20793
	// https://github.com/stolk/GPGOAP

	std::cout << "goapTest started \n";


	const action_planner_simple action_planner = create_fps_action_planner();
	

	const world_state_simple start(
		{
			{"enemyvisible", false},
			{"armedwithgun", true},
			{"weaponloaded", false},
			{"enemylinedup", false},
			{"enemyalive", true},
			{"armedwithbomb", true},
			{"nearenemy", false},
			{"alive", true}
		});
	const world_state_simple goal(
		{
			{"enemyalive", false},
			//             {"alive", true}
		});

	const a_star_plan_simple plan = a_star_plan_simple::make_plan(action_planner, start, goal).value();

	std::cout << "Results: \n";
	std::cout << plan << '\n';

	std::cout << "goapTest finished \n";
}


void goap_string_test()
{
	std::cout << "goapStringTest started \n";

	action_simple a("scout",
		{ {"armedwithgun", true} },
		{ {"enemyvisible", true} });

	std::cout << "Action: " << a << '\n';

	world_state_simple w({
		{"enemyvisible", false},
		{"armedwithgun", true},
		{"weaponloaded", false},
		{"enemylinedup", false},
		{"enemyalive", true},
		{"armedwithbomb", true},
		{"nearenemy", false},
		{"alive", true}
		});

	std::cout << "Worldstate: " << w << '\n';


	std::cout << "goapStringTest finished \n";
}