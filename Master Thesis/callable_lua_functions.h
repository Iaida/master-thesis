#pragma once

#include "sol.hpp"

// encapsulated lua functions to ensure proper parameters when called from c++ code
class callable_lua_functions
{
public:
	static std::string table_to_string(const sol::state_view & lua_state_view, const sol::table & table)
	{
		return lua_state_view["table"]["tostring"](table);
	}

	static sol::table table_deepcopy(const sol::state_view & lua_state_view, const sol::table & table)
	{
		return lua_state_view["table"]["deepcopy"](table);
	}

	static bool table_equality(const sol::state_view & lua_state_view, const sol::table & table_a, const sol::table & table_b)
	{
		return lua_state_view["isEqual"](table_a, table_b);
	}
};
