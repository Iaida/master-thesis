print("hook_example.lua start")
hook_utilities = require "hook_utilities"



function add(a, b)
	print("add called with " .. a .. " and " .. b)
	return a + b
end


add_hook("add", function(a, b)
	print("hook start")
	print("hook end, result = " .. add(a, b))
end)

activate_hooks()
add(2,3)
deactivate_hooks()
add(2,3)



print("hook_example.lua end")