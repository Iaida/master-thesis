#include <iostream>
#include <string_view>
#include "goap_tests.h"
#include "graph_tests.h"
#include "graph_goap_test.h"
#include "tic_tac_toe_tests.h"
#include "json_object.h"
#include "custom_json.h"
#include "rng.h"
#include "lua_tests.h"
#include "custom_json_tests.h"
#include "run_lua_with_goap.h"
#include "graphviz_lua.h"

int main()
{
	//goap_string_test();
	//goap_test();

	
	/*write_graphviz_example();
	label_writer_example();*/
	//static_graph_example();

	//dynamic_graph_example();

	//dynamic_graph_example_goap();

	//double_ai_tic_tac_toe_test();
	//player_vs_ai_tic_tac_toe_test();


	//json_test();

	//custom_json::village_test();

	
	//lua_test_string();
	//lua_test_conversions();

	//lua_test_api();
	//lua_test_action();

	//lua_test_table();

	
	//run_lua_file_with_goap("GoapWeekday.lua");
	//run_lua_file_with_goap("VillageExample.lua");
	
	//run_lua_file_with_goap("quest_budget.lua");
	//run_lua_file_with_goap("market_crier.lua");
	//run_lua_file_with_goap("deepcopy_test.lua");
	//run_lua_file_with_goap("location.lua");
	//run_lua_file_with_goap("table_function_parameter_test.lua");
	//run_lua_file_with_goap("reference_test.lua");

	//run_lua_file_with_goap("graph_example.lua");
	//run_lua_file_with_goap("hook_example.lua");

	
	//run_lua_file_with_goap("cyoa.lua");
	//run_lua_file_with_goap("tic_tac_toe.lua");
	run_lua_file_with_goap("combination.lua");

	system("pause");
	/*std::cin.get();*/
	return EXIT_SUCCESS;
}
