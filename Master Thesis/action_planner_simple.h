#pragma once

#include <vector>
#include <algorithm>
#include <set>

#include "action_simple.h"
#include "world_state_simple.h"

class action_planner_simple
{
public:
	action_planner_simple() = default;

	bool add_action(action_simple&& action)
	{
		if (action_exists(action))
			return false;

		actions_.emplace_back(action);

		for (string_id& atom : action.all_atoms())
		{
			atoms_.emplace(atom);
		}
		return true;
	}

	std::vector<std::pair<action_simple, world_state_simple>> possible_actions_and_states(const world_state_simple& from) const
	{
		std::vector<std::pair<action_simple, world_state_simple>> result;
		for (const action_simple& action : actions_)
		{
			std::optional<world_state_simple> opt_state = from.create_new_state_from_action(action);
			if (opt_state)
				result.emplace_back(std::make_pair(action, *opt_state));
		}
		return result;
	};


private:
	// for managing conversions to and from indices and performance improvements
	std::set<string_id> atoms_;
	std::vector<action_simple> actions_;

	bool action_exists(const action_simple& action) const
	{
		return std::find(std::cbegin(actions_), std::cend(actions_), action) != std::cend(actions_);
	}
};
