print("graph_hooks.lua start")
hook_utilities = require "hook_utilities"

local g = graphviz.new()

local function location_label(location_string, sublocation_string)
	return location_string .. "_" .. sublocation_string
end

local function add_village_part_hook(village_table, village_part_name)
	local node_label = location_label(village_table.name, village_part_name)
	g:add_vertex_to_graph(node_label)
end

local function create_one_way_location_connection_action_hook(source_location_table, target_location_table, action_cost)
	local from = location_label(source_location_table.village_name, source_location_table.name)
	local to = location_label(target_location_table.village_name, target_location_table.name)

	local edge_label = "from " .. from .. "\n to " .. to .. ", cost: " .. (action_cost or 1)
	g:add_edge_to_graph(edge_label, from, to)
end

function start_graph_hooks()
	add_hook("add_village_part", add_village_part_hook)
	add_hook("create_one_way_location_connection_action", create_one_way_location_connection_action_hook)
	activate_hooks()
end

function end_graph_hooks()
	deactivate_hooks()
	g:write_graph("graphviz_lua_test.gv")
end





print("graph_hooks.lua end")