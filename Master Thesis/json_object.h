// CANCELLED ATTEMPT at own json type, see 'custom_json' for correct usage of library

#pragma once

#include <vector>
#include <map>

#include "string_id.h"
#include "visitor.h"


#include "json.hpp" // https://github.com/nlohmann/json
#include "custom_json.h"


// https://stackoverflow.com/questions/48662597/initialize-a-map-of-stdvariant-to-represent-a-json-file
// https://github.com/asielorz/Json
// https://gist.github.com/willkill07/76268e7a88136705f7c2ea9177897cf1
// https://gitlab.com/cky/cpp17-json/tree/master/json
// -> for recursive implementation, works because of CRTP

// emulates structure of: https://www.json.org/

namespace json_object
{

struct json_object; // forward declaration
using string_t = string_id;
using array_t = std::vector<json_object>;
using object_t = std::map<string_t, json_object>;
using value_t = std::variant<
	std::monostate, // necessary for default constructor
	std::nullptr_t,
	double, int64_t, uint64_t,
	string_t,
	bool,
	array_t,
	object_t>;


// variant constructor for 'bool' and 'std::string' converts 'const char *' to bool
// -> need to use string_literals
// variant constructor does not convert integer values 
// -> need to use integer literal
// AVOID construction with value_t directly, use json_object instead

struct json_object : value_t
{
	// allows for nested initializer lists -> WHY ???
	using value_t::value_t;
	

	json_object() : value_t(std::in_place_type<std::monostate>) {}
	json_object(const char * value) : value_t(std::in_place_type<string_t>, value) {}
	json_object(const int value) : value_t(std::in_place_type<int64_t>, value) {}
	json_object(const unsigned value) : value_t(std::in_place_type<uint64_t>, value) {}
	json_object(const float value) : value_t(std::in_place_type<double>, value) {}



	json_object(value_t &value) : value_t(value) {}
	json_object(const value_t &value) : value_t(value) {}
/*
	json_object(object_t &value) : value_t(value) {}
	json_object(const object_t &value) : value_t(value) {}

	json_object(array_t &value) : value_t(value) {}
	json_object(const array_t &value) : value_t(value) {}






	json_object(json_object &&) = default;
	json_object(json_object const &) = default;

	json_object & operator=(json_object const &) = default;
	json_object & operator=(json_object &&) = default;

	json_object & operator=(object_t const & o)
	{
		*this = value_t(o);
		return *this;
	}
	json_object & operator=(object_t && o)
	{
		*this = std::move(o);
		return *this;
	}

	~json_object() = default;


	operator value_t &()
	{
		return *this;
	}
	operator const value_t &() const
	{
		return *this;
	}


	value_t & value()
	{
		return *this;
	}

	value_t const &	value() const
	{
		return *this;
	}*/


};

using default_json = nlohmann::json;

inline void to_json(default_json & j, const json_object & wrapper)
{
	// 'std::visit' function argument needs explicit cast 
	// to variant type 'value_t' before it can be used
	j = static_cast<value_t>(wrapper);
}

inline void from_json(const default_json & j, value_t & v);  // forward declaration

inline void from_json(const default_json & j, json_object & wrapper)
{
	wrapper = j.get<value_t>();
}

inline void to_json(default_json & j, const value_t & v)
{
	std::visit(
		visitor
		{
			[](const std::monostate) {},
			[&j](auto arg) { j = arg; }
		}, v);
}

inline void from_json(const default_json & j, string_id & v)
{
	// json::string_t -> string_id
	v = j.get<default_json::string_t>();
}

inline void from_json(const default_json& j, value_t & v)
{
	switch (j.type())
	{
	case nlohmann::detail::value_t::null:
		v = j.get<std::nullptr_t>();
		break;
	case nlohmann::detail::value_t::object:
		v = j.get<object_t>();
		break;
	case nlohmann::detail::value_t::array:
		v = j.get<array_t>();
		break;
	case nlohmann::detail::value_t::string:
		v = j.get<string_t>();
		break;
	case nlohmann::detail::value_t::boolean:
		v = j.get<bool>();
		break;
	case nlohmann::detail::value_t::number_integer:
		v = j.get<int64_t>();
		break;
	case nlohmann::detail::value_t::number_unsigned:
		v = j.get<uint64_t>();
		break;
	case nlohmann::detail::value_t::number_float:
		v = j.get<double>();
		break;
	default:
		break;
	}

}

inline void to_json(default_json& j, const object_t & o)
{
	// iterate through object map's key-value-pairs
	for (const auto &[key, value] : o)
	{
		j[key.to_string()] = value;
	}
}

inline void from_json(const default_json& j, object_t & o)
{
	// iterate through json's key-value-pairs
	for (const auto & [key, value] : j.items())
	{
		o[string_id(key)] = value.get<json_object>();
	}
}


// does not keep member order, sorts alphabetically
inline void json_object_test()
{
	const json_object def;
	const json_object n = nullptr;
	const json_object b = true;
	const json_object d = 1.23;
	const json_object f = 1.23f;
	const json_object i = -123;
	const json_object u = 123u;
	const json_object s = "hello1";
	const json_object a1 = array_t{ b, d, s };


	const json_object nested_object = object_t
	{
		{
			"first",
			object_t
			{
				{"name", "first_object"},
				{"number", 1},
				{"array1", array_t{ b, d, s }}
			}
		},
		{
			"second",
			object_t
			{
				{"name", "second_object"},
				{"number", 2},
				{"array2", array_t{ a1, a1, a1 }}
			}
		}
	};

	// object_t -> json
	const default_json nested_json(nested_object);
	// print json to console
	std::cout << nested_json << '\n';

	// json -> file
	write_json_to_file(nested_json, "pretty.json");
	// file -> json
	default_json read_json;
	read_json_from_file(read_json, "pretty.json");
	// json -> file
	write_json_to_file(read_json, "pretty copy.json");

	// json -> json_object
	json_object from_json_test = read_json;
	// json -> file
	write_json_to_file(default_json(from_json_test), "pretty parsed.json");



	std::cout << "json_object_test finished \n";
}

}