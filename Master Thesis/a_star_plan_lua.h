#pragma once

#include <vector>
#include <iostream>

#include "action_lua.h"
#include "action_planner_lua.h"
#include "world_state_lua.h"
#include "a_star_node_lua.h"


class a_star_plan_lua
{
public:

	// http://theory.stanford.edu/~amitp/GameProgramming/ImplementationNotes.html
	// https://www.redblobgames.com/pathfinding/a-star/introduction.html
	// https://www.redblobgames.com/pathfinding/a-star/implementation.html#cpp-astar
	// AI missionaries and cannibals
	// https://en.wikipedia.org/wiki/A*_search_algorithm

	// TODO: cheapest plan with multiple goals
	// TODO: optional max iterations for testing endless loops
	// TODO: randomness when cost same
	static std::optional<a_star_plan_lua> make_plan(const action_planner_lua& action_planner, const sol::table & start_table, const sol::function& goal_conditions)
	{
		const world_state_lua start(start_table);

		// The set of currently discovered nodes that are not evaluated yet.
		// Initially, only the start node is known.
		// not using 'std::priority_queue' and its heap properties because iteration (begin+end) is needed
		// TODO: might be able to use 'std::make_heap' etc as an alternative
		std::vector<a_star_node_lua> opened;
		// The set of nodes already evaluated
		std::vector<a_star_node_lua> closed;

		// add start node
		opened.emplace_back(a_star_node_lua(start, world_state_lua(), action_lua(), 0, goal_conditions));

		while (!opened.empty())
		{
			// find node with lowest f in 'opened'

			auto lowest_cost_node_it = std::min_element(std::cbegin(opened), std::cend(opened),
				[](const a_star_node_lua& a, const a_star_node_lua& b)
			{
				return a.f() < b.f();
			});
			a_star_node_lua current_node = *lowest_cost_node_it;
			opened.erase(lowest_cost_node_it);

			// check if state achieved 'goal'
			if (current_node.current_state().all_conditions_achieved(goal_conditions))
			{
				return a_star_plan_lua(action_planner, start, goal_conditions, current_node, closed);
			}

			// add 'currentNode' to closed
			closed.emplace_back(current_node);

			// iterate over neighbors of 'currentNode'
			for (const auto&[action, state] : action_planner.possible_actions_and_states(current_node.current_state()))
			{
				a_star_node_lua neighbor(state, current_node.current_state(), action, current_node.g(), goal_conditions);

				auto node_with_same_state_in_opened_it = std::find_if(std::cbegin(opened), std::cend(opened),
					[&neighbor](const a_star_node_lua& n)
				{
					return n.current_state() == neighbor.current_state();
				});

				auto node_with_same_state_in_closed_it = std::find_if(std::cbegin(closed), std::cend(closed),
					[&neighbor](const a_star_node_lua& n)
				{
					return n.current_state() == neighbor.current_state();
				});

				// if 'neighbor' state in 'opened' and cost g of 'neighbor' less than the node in 'opened':
				if (node_with_same_state_in_opened_it != std::cend(opened) &&
					neighbor.g() < node_with_same_state_in_opened_it->g())
				{
					// remove neighbor from 'opened', because new path is better
					opened.erase(node_with_same_state_in_opened_it);
					// neighbor is now no longer in 'opened', update 'nodeWithSameStateInOpenedIt' for following check
					node_with_same_state_in_opened_it = std::cend(opened);
				}

				// if 'neighbor' state in 'closed' and cost g of 'neighbor' less than the node in 'closed':
				if (node_with_same_state_in_closed_it != std::cend(closed) &&
					neighbor.g() < node_with_same_state_in_closed_it->g())
				{
					// remove neighbor from 'closed'
					closed.erase(node_with_same_state_in_closed_it);
					// neighbor is now no longer in 'closed', update 'nodeWithSameStateInClosedIt' for following check
					node_with_same_state_in_closed_it = std::cend(closed);
				}

				// if neighbor state not in OPEN and neighbor not in CLOSED:
				if (node_with_same_state_in_opened_it == std::cend(opened) &&
					node_with_same_state_in_closed_it == std::cend(closed))
				{
					// add 'neighbor' to 'opened'
					opened.emplace_back(neighbor);
				}
			}
		}

		// did not find a path
		return std::nullopt;
	}

	

	std::pair<action_lua, world_state_lua> first_action_state_pair() const
	{
		// constructed plan should always have at least 1 action and at least 2 world_states
		return std::make_pair(actions_[0], world_states_[1]);
	}

	// version is only callable from lua
	// returns {action, state} table
	sol::table first_action_state_table(sol::this_state lua_state) const
	{
		sol::state_view lua = lua_state;

		if (actions_.empty() || world_states_.size() < 2)
			return sol::nil;

		return lua.create_table_with(
			"action", actions_[0].table(),
			"state", world_states_[1].table()
		);
	}

	std::string to_string() const
	{
		std::string result;
		result += "AStarPlan: cost: " + std::to_string(total_cost_) + "\n";

		for (size_t i = 0; i < world_states_.size(); i++)
		{
			result += "WorldState " + std::to_string(i) + ": " + world_states_[i].to_string() + "\n";
			if (i < actions_.size())
				result += "Action     " + std::to_string(i) + ": " + actions_[i].to_string() + "\n";
		}
		return result;
	}

	bool operator<(const a_star_plan_lua& rhs) const
	{
		return total_cost_ < rhs.total_cost_;
	}

private:

	// input values only for debugging purposes, not many plan instances atm, so no performance problem
	action_planner_lua action_planner_;
	world_state_lua start_;
	sol::function goal_conditions_;

	// TODO : better symmetry, maybe pair<action_lua,state>?, include start
	// results
	std::vector<world_state_lua> world_states_; // 1 more element than actions, includes start
	std::vector<action_lua> actions_; // 1 less element than worldStates
	float total_cost_ = FLT_MAX;


	a_star_plan_lua(const action_planner_lua& action_planner, const world_state_lua& start, const sol::function& goal_conditions_,
		const a_star_node_lua& goal_node, const std::vector<a_star_node_lua>& closed) :
		action_planner_(action_planner), start_(start), goal_conditions_(goal_conditions_)
	{
		reconstruct(goal_node, closed);
	}

	void reconstruct(const a_star_node_lua& goal_node, const std::vector<a_star_node_lua>& closed)
	{
		total_cost_ = goal_node.g();

		// start at 'goalNode'
		world_states_.emplace_back(goal_node.current_state());

		a_star_node_lua current_node = goal_node;

		while (true)
		{
			action_lua previous_action = current_node.previous_action();
			// check if start node reached, which has default values for previous action and world state
			if (previous_action == action_lua())
				break;

			actions_.emplace_back(previous_action);
			world_state_lua previous_state = current_node.previous_state();
			world_states_.emplace_back(previous_state);


			auto node_with_same_state_in_closed_it = std::find_if(std::cbegin(closed), std::cend(closed),
				[&previous_state](const a_star_node_lua& n)
			{
				return n.current_state() == previous_state;
			});

			if (node_with_same_state_in_closed_it != std::cend(closed))
				current_node = *node_with_same_state_in_closed_it;
			else
				break;
		}

		// reconstruction was done from goal to start
		// -> reverse containers
		std::reverse(world_states_.begin(), world_states_.end());
		std::reverse(actions_.begin(), actions_.end());
	}


	friend std::ostream& operator<<(std::ostream& stream, const a_star_plan_lua& plan)
	{
		return stream << plan.to_string();
	}


};

