#pragma once
#include <random>
#include <ctime>

namespace rng
{
// constructed with seed
inline std::mt19937_64 engine(time(nullptr));

inline float random_float(const float min, const float max)
{
	return std::uniform_real_distribution<float>(min, max)(engine);
}

inline size_t random_unsigned(const size_t min, const size_t max)
{
	return std::uniform_int_distribution<size_t>(min, max)(engine);
}

inline bool random_bool(const double true_probability)
{
	return std::bernoulli_distribution(true_probability)(engine);
}

inline bool coin_flip_bool()
{
	return random_bool(0.5);
}


inline std::string random_hiragana_name()
{
	// https://www.coscom.co.jp/hiragana-katakana/kanatable.html
	const std::vector<std::string> hiragana =
	{
		"a",  "i",   "u",   "e",  "o",
		"ka", "ki",  "ku",  "ke", "ko",
		"ga", "gi",  "gu",  "ge", "go",
		"sa", "shi", "su",  "se", "so",
		"za", "ji",  "zu",  "ze", "zo",
		"ta", "chi", "tsu", "te", "to",
		"da", "ji",  "zu",  "de", "do",
		"na", "ni",  "nu",  "ne", "no",
		"ha", "hi",  "hu",  "he", "ho",
		"ba", "bi",  "bu",  "be", "bo",
		"pa", "pi",  "pu",  "pe", "po",
		"ma", "mi",  "mu",  "me", "mo",
		"ya",        "yu",        "yo",
		"ra", "ri",  "ru",  "re", "ro",
		"wa",                     "o",
		"n"
	};

	std::string out;
	const size_t n = random_unsigned(2, 3);
	for (size_t i = 0; i < n; i++)
	{
		const size_t index = random_unsigned(0, hiragana.size() - 1);
		out += hiragana[index];
	}
	return out;
}
}
