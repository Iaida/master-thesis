local function_name_hook_table = {}

-- based on https://stackoverflow.com/questions/10458306/get-name-of-argument-of-function-in-lua/10458912#10458912
function hook()
	
    -- passing 2 to to debug.getinfo means 'give me info on the function that spawned 
    -- this call to this function'. level 1 is the C function that called the hook.
    local info = debug.getinfo(2)
    if info ~= nil and info.what == "Lua" then
        local i = 1
		local variables = {}
        -- now run through all the local variables at this level of the lua stack
        while true do
            local name, value = debug.getlocal(2, i)
            if name == nil then
                break
            end
            -- this just skips unused variables
            if name ~= "(*temporary)" then
                variables[#variables+1] = value
            end
            i = i + 1
        end

		if info.name ~= nil and function_name_hook_table[info.name] ~= nil then
			-- call hook function for current function name from 'function_name_hook_table' with same parameters
			function_name_hook_table[info.name](table.unpack(variables))
		end
    end
end

-- hook_function needs to use same parameters
function add_hook(function_name, hook_function)
	function_name_hook_table[function_name] = hook_function
end

function activate_hooks()
	-- tell the debug library to call lua function 'hook 'every time a function call is made
	debug.sethook(hook, "c")
end

function deactivate_hooks()
	debug.sethook()
end

