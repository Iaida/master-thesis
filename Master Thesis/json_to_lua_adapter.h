#pragma once
#define SOL_CHECK_ARGUMENTS 1

#include "sol.hpp"
#include "custom_json.h"
#include <iostream>
#include <memory>



// necessary to wrap 'custom_json' to avoid compiler errors which occur when using 'custom_json' by itself
struct json_to_lua_adapter
{
	custom_json json;
};
