#pragma once

#include <map>
#include <cctype>
#include <optional>
#include <algorithm>
#include "action_simple.h"

class world_state_simple
{
public:
	world_state_simple() = default;

	world_state_simple(const std::map<string_id, bool>& states) :
		states_(states)
	{
	}

	std::optional<world_state_simple> create_new_state_from_action(const action_simple& action) const
	{
		return preconditions_achieved(action.preconditions())
			? std::optional<world_state_simple>(world_state_simple(*this, action.effects()))
			: std::nullopt;
	}

	std::string to_string() const
	{
		std::string result;
		for (const auto&[atom, value] : states_)
		{
			// positive atoms are written in upper case, negative atoms in lower case
			result += (value ? to_upper(atom.to_string()) : atom.to_string()) + ' ';
		}
		return result;
	}

	std::string formatted_multiline_string() const
	{
		std::string result;
		for (const auto&[atom, value] : states_)
		{
			// positive atoms are written in upper case, negative atoms in lower case
			result += (value ? to_upper(atom.to_string()) : atom.to_string()) + '\n';
		}
		return result;
	}

	// returns the number of atoms which are mismatched or not present
	float distance_to_goal(const world_state_simple& goal) const
	{
		return static_cast<float>(std::count_if(std::cbegin(goal.states_), std::cend(goal.states_),
			[&](const auto& atom_value_pair)
		{
			return !precondition_achieved(atom_value_pair);
		}));
	}

	bool goal_reached(const world_state_simple& goal) const
	{
		return distance_to_goal(goal) == 0.0f;
	}

	bool any_goal_reached(const std::vector<world_state_simple> & goals) const
	{
		return std::any_of(std::cbegin(goals), std::cend(goals),
			[&](const world_state_simple& goal) -> bool
		{
			return goal_reached(goal);
		});
	}

	const std::map<string_id, bool>& states() const
	{
		return states_;
	}

	bool operator==(const world_state_simple& rhs) const
	{
		return states_ == rhs.states_;
	}

	friend std::ostream& operator<<(std::ostream& out, const world_state_simple& ws)
	{
		return out << ws.to_string();
	}

	// implementation needed for boost::graph properties
	friend std::istream& operator>>(std::istream& in, world_state_simple& ws)
	{
		std::string string;
		auto & result = in >> string;
		ws = world_state_simple();
		return result;
	}


private:
	std::map<string_id, bool> states_;

	world_state_simple(const world_state_simple& previous_state, const std::map<string_id, bool>& effects) :
		states_(previous_state.states_)
	{
		apply_effects(effects);
	}

	std::string to_upper(std::string s) const
	{
		std::transform(s.begin(), s.end(), s.begin(), std::toupper);
		return s;
	}

	bool preconditions_achieved(const std::map<string_id, bool>& preconditions) const
	{
		return std::all_of(std::cbegin(preconditions), std::cend(preconditions),
			[&](const auto& precondition)
		{
			return precondition_achieved(precondition);
		});
	}

	bool precondition_achieved(const std::pair<const string_id, bool>& precondition) const
	{
		return std::find(std::cbegin(states_), std::cend(states_), precondition) != std::cend(states_);
	}


	void apply_effects(const std::map<string_id, bool>& effects)
	{
		for (const auto&[atom, value] : effects)
		{
			states_[atom] = value;
		}
	}
};

