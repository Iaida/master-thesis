-- https://www.reddit.com/r/lua/comments/417v44/efficient_table_comparison/
-- https://bitbucket.org/snippets/marcotrosi/XnyRj/lua-isequal

function isEqual(a,b)

   local function isEqualTable(t1,t2)

      -- reference comparison
      if t1 == t2 then
         return true
      end

	  -- iterate through keys of t1
      for k,v in pairs(t1) do
	     -- type comparison
         if type(t1[k]) ~= type(t2[k]) then
            return false
         end

         if type(t1[k]) == "table" then
		    -- nested table, recursive call
            if not isEqualTable(t1[k], t2[k]) then
               return false
            end
         else
		    -- not a nested table, value comparison
            if t1[k] ~= t2[k] then
               return false
            end
         end
      end

	  -- iterate through keys of t2
      for k,v in pairs(t2) do

         if type(t2[k]) ~= type(t1[k]) then
            return false
         end

         if type(t2[k]) == "table" then
            if not isEqualTable(t2[k], t1[k]) then
               return false
            end
         else
            if t2[k] ~= t1[k] then
               return false
            end
         end
      end

      return true
   end

   -- type comparison
   if type(a) ~= type(b) then
      return false
   end

   if type(a) == "table" then
      -- table comparison
      return isEqualTable(a,b)
   else
      -- value comparison
      return (a == b)
   end

end




function table_equality_test()
	print("table_equality called")
	---[[
	local tableA = 
	{
		["One"] = 1,
		["Two"] = 2,
		["Three"] = 3
	}

	local tableB = 
	{
		["One"] = 1,
		["Two"] = 2,
		["Three"] = 3
	}

	local tableC = tableA
	--]]
	print("Same values: " .. tostring(tableA == tableB))
	print("Assignment: " .. tostring(tableA == tableC))

	print("equality function: " .. tostring(isEqual(tableA, tableB)))
end