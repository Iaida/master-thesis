#pragma once

#include <string>
#include <map>
#include <vector>
#include <algorithm>
#include <istream>
#include <optional>

class string_id
{
public:
	// for nlohmann::json
	using value_type = std::string::value_type;

	string_id() : string_id("")	{}

	string_id(size_t n, char c) : string_id(std::string(n, c)) {}

	string_id(const std::string& string)
	{
		// check if already present in map
		const auto it = std::find_if(std::cbegin(id_string_map_), std::cend(id_string_map_),
		                             [&string](const std::pair<size_t, std::string> pair)
		                             {
			                             return pair.second == string;
		                             });

		if (it == std::cend(id_string_map_))
		{
			id_ = counter_++;
			id_string_map_[id_] = string;
		}
		else
		{
			id_ = it->first;
		}
	}


	// for easier construction
	string_id(const char* char_array) : string_id(std::string(char_array))
	{
	}

	// return by value would lead to errors
	const std::string & to_string() const
	{
		return id_string_map_[id_];
	}

	bool operator==(const string_id& rhs) const
	{
		return id_ == rhs.id_;
	}

	bool operator<(const string_id& rhs) const
	{
		return id_ < rhs.id_;
	}

	size_t get_id() const
	{
		return id_;
	}

	explicit operator size_t() const
	{
		return id_;
	}

	explicit operator std::string() const
	{
		return to_string();
	}

	operator const std::string &() const
	{
		return to_string();
	}


	friend std::ostream& operator<<(std::ostream& out, const string_id& sid)
	{
		return out << sid.to_string();
	}

	// implementation needed for boost::graph properties
	friend std::istream& operator>>(std::istream& in, string_id& sid)
	{
		std::string string;
		auto & result = in >> string;
		sid = string_id(string);
		return result;
	}

	// for nlohmann::json
	size_t size() const
	{
		return to_string().size();
	}

	// for nlohmann::json
	void resize(size_t n, char c)
	{
		std::string s = to_string();
		s.resize(n, c);
		*this = string_id(s);
	}

	// for nlohmann::json
	const char* c_str() const noexcept
	{
		return to_string().c_str();
	}

	// for nlohmann::json
	const char& operator[] (size_t pos) const
	{
		return to_string()[pos];
	}

	// for nlohmann::json
	const char& back() const
	{
		return to_string().back();
	}

	// for nlohmann::json
	const char* data() const noexcept
	{
		return to_string().data();
	}

	// for nlohmann::json
	void push_back(char c)
	{
		std::string s = to_string();
		s.push_back(c);
		*this = string_id(s);
	}

	// for nlohmann::json
	void clear() noexcept
	{
		*this = string_id("");
	}

	void append(const char* s, size_t n)
	{
		std::string string = to_string();
		string.append(s, n);
		*this = string_id(string);
	}

	static std::optional<string_id> from_id(const size_t id)
	{
		if (id_string_map_.find(id) == std::cend(id_string_map_))
			return std::nullopt;

		string_id result;
		result.id_ = id;
		return result;
	}


private:
	static size_t counter_;
	static std::map<size_t, std::string> id_string_map_;

	size_t id_;
};

// initialize static member variables
size_t string_id::counter_ = 0;
std::map<size_t, std::string> string_id::id_string_map_ = std::map<size_t, std::string>();

// http://en.cppreference.com/w/cpp/utility/hash
namespace std
{
	template <>
	struct hash<string_id>
	{
		std::size_t operator()(const string_id& key) const noexcept
		{
			return hash<size_t>()(key.get_id());
		}
	};
}
