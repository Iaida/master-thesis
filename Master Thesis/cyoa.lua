print("cyoa.lua start")

LuaUtilities = require "LuaUtilities"

page_messages = 
{
	"There is a big red button in front of you with a sign above it. To the north there is a heavy steel door embedded in the wall.",
	nil,
	"As you press the button, the whole building explodes and you are buried alive inside of the debris.",
	"After you've entered the code, a green light flashes and the door unlocks.",
	"The door is locked, but you spot a small panel next to it with a display that reads 'Enter Code'.",
	"The sign reads: 'The code for the door is '4'. Better avoid pressing the red button.'.",
	"You've escaped!"
}

start_page_1 = 
{
	page = 1,
	message = page_messages[1]
}

function turn_to_page_effect(page_number)
	return function(result)
		result.message = page_messages[page_number]
		result.page = page_number
		return result
	end
end

local ap = action_planner.new()
ap:add_action_array(
{
	{
		name = "Press the red button.",
		cost = 1.0,
		preconditions = function(state_table)
			return { state_table.page == 1 }
		end,
		effect = function(state_table)
			local result = turn_to_page_effect(3)(state_table)
			result.finished = true
			return result
		end
	},
	{
		name = "Read the sign.",
		cost = 1.0,
		preconditions = function(state_table)
			return { state_table.page == 1 }
		end,
		effect = function(state_table)
			local result = turn_to_page_effect(6)(state_table)
			result.sign_read = true
			return result
		end
	},
	{
		name = "Try to open the door.",
		cost = 1.0,
		preconditions = function(state_table)
			return { state_table.page == 1 }
		end,
		effect = turn_to_page_effect(5)
	},
	{
		name = "Play again.",
		cost = 1.0,
		preconditions = function(state_table)
			return { get_table_flag(state_table, "finished") }
		end,
		effect = function(state_table)
			local result = table.deepcopy(start_page_1)
			return result
		end
	},
	{
		name = "Leave the room.",
		cost = 1.0,
		preconditions = function(state_table)
			return { state_table.page == 4 }
		end,
		effect = function(state_table)
			local result = turn_to_page_effect(7)(state_table)
			result.finished = true
			result.escaped = true
			return result
		end
	},
	{
		name = "Ignore the warning and still press the red button.",
		cost = 1.0,
		preconditions = function(state_table)
			return { state_table.page == 6 }
		end,
		effect = function(state_table)
			local result = turn_to_page_effect(3)(state_table)
			result.finished = true
			return result
		end
	},
	{
		name = "Enter Code.",
		cost = 1.0,
		preconditions = function(state_table)
			return { state_table.page == 5 and get_table_flag(state_table, "sign_read")}
		end,
		effect = turn_to_page_effect(4)
	},
	{
		name = "Look around the room.",
		cost = 1.0,
		preconditions = function(state_table)
			return { state_table.page == 5 and not get_table_flag(state_table, "sign_read")}
		end,
		effect = turn_to_page_effect(1)
	},
	{
		name = "Take closer look at the door.",
		cost = 1.0,
		preconditions = function(state_table)
			return { state_table.page == 6 }
		end,
		effect = turn_to_page_effect(5)
	}
})

---[[
print("creating plan... ")

goal_conditions = function(state_table)
	return 
	{
		get_table_flag(state_table, "escaped")
	}
end;

opt_plan = a_star_plan.make_plan(ap, start_page_1, goal_conditions)
print("opt_plan tostring: ")
print(tostring(opt_plan))
--]]

print("You are locked inside the Control Room. How do you escape?")

local quitting = false

current_state = table.deepcopy(start_page_1)

repeat
	local ast = ap:possible_actions_and_states_table(current_state)

	-- append action names to 'current_state.message'
	local message_to_concat = {current_state.message}
	for i, action_state_pair in ipairs(ast) do
		message_to_concat[#message_to_concat+1] = tostring(i) .. ": ".. action_state_pair.action.name
	end
	local message = table.concat(message_to_concat, "\n")

	-- array indices are the player input
	local valid_input = index_strings(#ast)
	-- allow quitting as valid input
	valid_input[#valid_input+1] = "q"

	-- print and read user input
	local input = read_and_return_valid_input(message, valid_input)
	if (input == "q") then 
		print("q input")
		quitting = true
		do break end
	end

	-- use input to choose action and assign new state
	local chosen_index = tonumber(input)
	current_state = ast[chosen_index].state

until quitting

print("quitting...")

print("cyoa.lua end")