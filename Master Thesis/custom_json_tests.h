#pragma once
#include <iostream>

#include "custom_json.h"


// keeps member order
inline void custom_json_object_test()
{
	const custom_json n = nullptr;
	const custom_json b = true;
	const custom_json d = 1.23;
	const custom_json f = 1.23f;
	const custom_json i = -123;
	const custom_json u = 123u;
	const custom_json s = "hello1";
	const custom_json a1 = custom_json::array_t{ b, d, s };


	const custom_json nested_json =
	{
		{
			"first",
			custom_json::object_t
			{
				{"name", "first_object"},
				{"number", 1},
				{"array1", custom_json::array_t{ b, d, s }}
			}
		},
		{
			"second",
			custom_json::object_t
			{
				{"name", "second_object"},
				{"number", 2},
				{"array2", custom_json::array_t{ a1, a1, a1 }}
			}
		}
	};

	// print json to console
	std::cout << nested_json << '\n';

	// json -> file
	write_json_to_file(nested_json, "pretty.json");


	// file -> json
	custom_json read_json;
	read_json_from_file(read_json, "pretty.json");

	// json -> file
	write_json_to_file(read_json, "pretty copy.json");

	// json -> object_t
	custom_json::object_t from_json_test = read_json.get<custom_json::object_t>();
	// json -> file
	write_json_to_file(custom_json(from_json_test), "pretty parsed.json");

	std::cout << "custom_json_object_test finished \n";
}