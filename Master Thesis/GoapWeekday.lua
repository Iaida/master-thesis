
print("GoapWeekday.lua start")

LuaUtilities = require "LuaUtilities"


increment_date_action =
{
	["name"] = "increment_date";
	["cost"] = 1.0;
	

	["preconditions"] = function(state_table)
		local switch_table = 
		{
			["monday"] = true,
			["tuesday"] = true,
			["wednesday"] = true,
			["thursday"] = true,
			["friday"] = true,
			["saturday"] = true,
			["sunday"] = true
		}
		local valid_day = switch_table[state_table["weekday"]]
		return 
		{
			valid_day
		}
	end;


	["effect"] = function(state_table)
		local switch_table = 
		{
			["monday"] = "tuesday",
			["tuesday"] = "wednesday",
			["wednesday"] = "thursday",
			["thursday"] = "friday",
			["friday"] = "saturday",
			["saturday"] = "sunday",
			["sunday"] = "monday"
		}
		local result = table.deepcopy(state_table)
		result["weekday"] = switch_table[state_table["weekday"]]
		return result
		
	end;

}

goal_conditions = function(state_table)
	return 
	{
		state_table["weekday"] == "sunday",
	}
end;

start_table = 
{
	["weekday"] = "monday"
}


local ap = action_planner.new({	increment_date_action })

print("creating plan... ")
opt_plan = a_star_plan.make_plan(ap, start_table, goal_conditions)
print("opt_plan tostring: ")
print(tostring(opt_plan))



print("GoapWeekday.lua end")