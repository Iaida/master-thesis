#pragma once
#include <string>
#include <fstream>

#include "json.hpp" // https://github.com/nlohmann/json
#include "string_id.h"

using custom_json = nlohmann::basic_json<std::map, std::vector, string_id, bool, std::int64_t, std::uint64_t, double>;


template<typename J>
void write_json_to_file(const J & j, const std::string & filename)
{
	// write prettified JSON to file
	std::ofstream file_out(filename);
	file_out << std::setw(4) << j << std::endl;
}

template<typename J>
void read_json_from_file(J & j, const std::string & filename)
{
	// read a JSON file
	std::ifstream file_in(filename);
	file_in >> j;
}