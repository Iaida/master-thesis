#pragma once

#include "action_simple.h"
#include "world_state_simple.h"


class a_star_node_simple
{
public:
	a_star_node_simple(const world_state_simple& current_state, const world_state_simple& previous_state, const action_simple& previous_action,
	            const float previous_cost, const world_state_simple& goal_state) :
		current_state_(current_state), previous_state_(previous_state), previous_action_(previous_action)
	{
		g_ = previous_cost + previous_action.cost();
		h_ = current_state.distance_to_goal(goal_state);
		f_ = g_ + h_;
	}

	float f() const
	{
		return f_;
	}

	float g() const
	{
		return g_;
	}

	const world_state_simple& current_state() const
	{
		return current_state_;
	}

	const world_state_simple& previous_state() const
	{
		return previous_state_;
	}

	const action_simple& previous_action() const
	{
		return previous_action_;
	}


private:
	// cost of the path from the start node to 'currentState'
	float g_;
	// heuristic that estimates the cost of the cheapest path from 'currentState' to the goal
	float h_;
	// g + h
	float f_;

	world_state_simple current_state_;
	world_state_simple previous_state_;
	action_simple previous_action_;
};
