print("graph_example.lua start")

local g = graphviz.new()
g:add_vertex_to_graph("a")
g:add_vertex_to_graph("b")
g:add_vertex_to_graph("c")
g:add_edge_to_graph("1", "a", "b")
g:add_edge_to_graph("2", "b", "a")
g:add_edge_to_graph("3", "a", "c")

g:write_graph("graphviz_lua_test.gv")

print("graph_example.lua end")