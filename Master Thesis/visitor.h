// https://www.youtube.com/watch?v=EsUmnLgz8QY

#pragma once
#include <variant>

template <typename ... Base>
struct visitor : Base...
{
	using Base::operator()...;
};

template <typename ... T>
visitor(T...) -> visitor<T...>;
