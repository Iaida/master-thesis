#pragma once
#include <boost/graph/graphviz.hpp>

#include "goap_tests.h"
#include "a_star_plan_simple.h"

inline void dynamic_graph_example_goap()
{
	using namespace boost;

	struct vertex_props
	{
		world_state_simple world_state;
		std::string formatted_label;
	};

	struct edge_props
	{
		action_simple action;
	};


	using custom_graph = adjacency_list<setS, setS, directedS, vertex_props, edge_props>;
	custom_graph graph;

	const action_planner_simple action_planner = create_fps_action_planner();

	const world_state_simple start(
		{
			{"enemyvisible", false},
			{"armedwithgun", true},
			{"weaponloaded", false},
			{"enemylinedup", false},
			{"enemyalive", true},
			{"armedwithbomb", true},
			{"nearenemy", false},
			{"alive", true}
		});
	const world_state_simple goal(
		{
			{"enemyalive", false},
			//             {"alive", true}
		});


	const vertex_props start_vertex_props = { start, start.formatted_multiline_string() };

	const custom_graph::vertex_descriptor start_vertex = add_vertex(start_vertex_props, graph);

	auto current_vertex = start_vertex;

	
	while (true)
	{
		vertex_props current_vertex_properties = graph[current_vertex];

		if (current_vertex_properties.world_state.goal_reached(goal))
		{
			std::cout << "Reached goal state \n";
			break;
		}

		const a_star_plan_simple plan = a_star_plan_simple::make_plan(action_planner, current_vertex_properties.world_state, goal).value();
		// add edge and vertex
		const auto[first_action, first_state] = plan.first_action_state_pair();
		const auto target_vertex = add_vertex({ first_state, first_state.formatted_multiline_string() }, graph);
		const auto add_edge_result = add_edge(current_vertex, target_vertex, { first_action }, graph);
		std::cout << "add_edge_result: " << add_edge_result.second << '\n';

		current_vertex = target_vertex;
	}

	std::ofstream dynamic_graph_example_dot("dynamic_graph_example_goap.gv");

	// https://stackoverflow.com/questions/45426129/boostwrite-graphviz-how-to-generate-the-graph-horizontally
	dynamic_properties dp;
	dp.property("node_id", get(&vertex_props::world_state, graph));
	dp.property("label", get(&vertex_props::formatted_label, graph));
	dp.property("label", get(&edge_props::action, graph));
	dp.property("rankdir", boost::make_constant_property<custom_graph*>(std::string("LR")));

	write_graphviz_dp(dynamic_graph_example_dot, graph, dp);
}
