-- TODO : Lua 'member' function syntax with ':' for things such as agent table
-- TODO : no starting weapon, get from forge, would need indicator for players

print("combination.lua start")

combination_dsl = require "combination_dsl"
graph_hooks = require "graph_hooks"

init_random()

local start_state = 
{
	player = 
	{
		location = {},
		quest_budget = 0,
		quests_in_progress = {},
		quests_completed = {},
		has_weapon = true,
		hunger = 0,
		currency = 10
	},

	villages = {},

	market_criers = {},
}

local location_actions = {}

start_graph_hooks()

-- URBAN VILLAGE
---[[
local urban_village = add_village("urban village", start_state.villages)

local urban_market = add_village_part(urban_village, "market")
local urban_church = add_village_part(urban_village, "church")
local urban_forge = add_village_part(urban_village, "forge")
local urban_forest = add_village_part(urban_village, "forest")
local urban_castle = add_village_part(urban_village, "castle")

add_two_way_location_connection_action(location_actions, urban_market, urban_church)
add_two_way_location_connection_action(location_actions, urban_market, urban_forge)
add_two_way_location_connection_action(location_actions, urban_market, urban_forest)
add_two_way_location_connection_action(location_actions, urban_forest, urban_castle)
--]]

-- RURAL VILLAGE
local rural_village = add_village("rural village", start_state.villages)

local rural_market = add_village_part(rural_village, "market")
local rural_fields = add_village_part(rural_village, "fields")
local rural_farm = add_village_part(rural_village, "farm")
local rural_mill = add_village_part(rural_village, "mill")
local rural_forest = add_village_part(rural_village, "forest")

add_two_way_location_connection_action(location_actions, rural_market, rural_farm)
add_two_way_location_connection_action(location_actions, rural_market, rural_mill)
add_two_way_location_connection_action(location_actions, rural_market, rural_forest)
add_two_way_location_connection_action(location_actions, rural_market, rural_fields)
add_two_way_location_connection_action(location_actions, rural_fields, rural_farm)
add_two_way_location_connection_action(location_actions, rural_fields, rural_mill)

-- connects both villages
add_two_way_location_connection_action(location_actions, rural_forest, urban_forest)

end_graph_hooks()

-- food related action definitions
local obtaining_food_actions = {}

local rural_market_food_cost = 1
obtaining_food_actions[#obtaining_food_actions+1] = create_action(
	"buys food at rural market and eats",
	function(state_table)
		return 
		{
			same_location(state_table.current_agent_table.location, rural_market),
			state_table.current_agent_table.currency >= rural_market_food_cost
		}
	end,
	function(state_table)
		state_table.current_agent_table.hunger = state_table.current_agent_table.hunger - 1
		state_table.current_agent_table.currency = state_table.current_agent_table.currency - rural_market_food_cost
		return state_table
	end
)

local urban_market_food_cost = 2
obtaining_food_actions[#obtaining_food_actions+1] = create_action(
	"buys food at urban market and eats",
	function(state_table)
		return 
		{
			same_location(state_table.current_agent_table.location, urban_market),
			state_table.current_agent_table.currency >= urban_market_food_cost
		}
	end,
	function(state_table)
		state_table.current_agent_table.hunger = state_table.current_agent_table.hunger - 2
		state_table.current_agent_table.currency = state_table.current_agent_table.currency - urban_market_food_cost
		return state_table
	end
)


obtaining_food_actions[#obtaining_food_actions+1] = create_action(
	"hunts for food in forest",
	function(state_table)
		return 
		{
			same_location(state_table.current_agent_table.location, urban_forest) or 
			same_location(state_table.current_agent_table.location, rural_forest),
			get_table_flag(state_table.current_agent_table, "has_weapon")
		}
	end,
	function(state_table)
		state_table.current_agent_table.hunger = state_table.current_agent_table.hunger - 5
		return state_table
	end
)

local weapon_cost = 3
obtaining_food_actions[#obtaining_food_actions+1] = create_action(
	"get weapon at forge",
	function(state_table)
		return 
		{
			same_location(state_table.current_agent_table.location, urban_forge),
			state_table.current_agent_table.currency >= weapon_cost,
			not get_table_flag(state_table.current_agent_table, "has_weapon")
		}
	end,
	function(state_table)
		state_table.current_agent_table.has_weapon = true
		state_table.current_agent_table.currency = state_table.current_agent_table.currency - weapon_cost
		return state_table
	end
)

-- action planner definitions

local villager_action_planner = action_planner.new()
villager_action_planner:add_action_array(location_actions)
villager_action_planner:add_action_array(obtaining_food_actions)


urban_village_action_planners = 
{
	create_job_action_planner(villager_action_planner, create_job_action(urban_church, 2)),
	create_job_action_planner(villager_action_planner, create_job_action(urban_forge, 2)),
	create_job_action_planner(villager_action_planner, create_job_action(urban_castle, 3)),
}

rural_village_action_planners = 
{
	create_job_action_planner(villager_action_planner, create_job_action(rural_fields, 1)),
	create_job_action_planner(villager_action_planner, create_job_action(rural_farm, 1)),
	create_job_action_planner(villager_action_planner, create_job_action(rural_mill, 1)),
}


local player_action_planner = action_planner.new()
player_action_planner:add_action_array(location_actions)
player_action_planner:add_action_array(obtaining_food_actions)

-- player agent definition
local player_agent_table = create_agent(player_action_planner, function(state_table)
	return state_table.player
end)
local start_location = rural_market
agent_set_location(player_agent_table, start_state, start_location)

local quest_world_changes = {}

local npc_agents = {}







-- urban agent definitions
local number_urban_villagers = 10

for i=1, number_urban_villagers do
	npc_agents[#npc_agents+1] = create_villager_agent(
		random_array_element(urban_village_action_planners), urban_village, 
		random_array_element(urban_village.village_parts))
end

-- rural agent definitions
local number_rural_villagers = 20

for i=1, number_rural_villagers do
	npc_agents[#npc_agents+1] = create_villager_agent(
		random_array_element(rural_village_action_planners), rural_village, 
		random_array_element(rural_village.village_parts))
end




-- wolves quest definition
local wolves_quest_tag = "wolves"
do
	local unlock_wolves_quest_action = create_quest_unlock_action(
		"approaches distressed farmer",
		"Farmer pleads: 'Wolves are attacking our villagers! Please help us!'",
		0, wolves_quest_tag, rural_market)

	
	player_action_planner:add_action(unlock_wolves_quest_action)

	-- some parts are too specific for encapsulation like 'create_quest_unlock_action'
	local wolves_quest_rep_gain = 5
	local complete_wolves_quest_player_action = create_action(
		"slays wolves with weapon",
		function(state_table)
			return 
			{
				same_location(state_table.current_agent_table.location, rural_forest),
				get_table_flag(state_table.player.quests_in_progress, wolves_quest_tag),
				get_table_flag(state_table.current_agent_table, "has_weapon")
			}
		end,
		function(state_table)
			set_table_flag(state_table.player.quests_in_progress, wolves_quest_tag, false)
			state_table.player.quests_completed[wolves_quest_tag] = true
			local current_village_name = state_table.player.location.village_name
			state_table.villages[current_village_name].reputation = state_table.villages[current_village_name].reputation + wolves_quest_rep_gain
			state_table.message = "You have slain the wolves!"
			return state_table
		end
	)

	player_action_planner:add_action(complete_wolves_quest_player_action)

	quest_world_changes[wolves_quest_tag] = function(state_table)
		if (get_table_flag(state_table.player.quests_in_progress, wolves_quest_tag)) then 
			-- kill npc in farm or forest
			local target_location = conditional_op(random_bool(), rural_farm, rural_forest)

			found_victim = false
			for _, current_agent in ipairs(npc_agents) do
				local agent_subtable = agent_get_subtable(current_agent, state_table)
				local at_same_location = same_location(agent_subtable.location, target_location)
				if at_same_location then
					agent_subtable.dead = true
					print(agent_subtable.name .. " was killed by wolves")
					found_victim = true
				end
			end

			if (found_victim) then
				print("The wolves retreat back into the forest")
			end

		end
		return state_table
	end

	
end

-- market crier action planner definition
local market_crier_action_planner = action_planner.new()
market_crier_action_planner:add_action_array(location_actions)
market_crier_action_planner:add_action_array(obtaining_food_actions)
market_crier_action_planner:add_action(
	create_exclamation_action(urban_market, 2))
market_crier_action_planner:add_action(
	create_exclamation_action(rural_market, 1))

market_crier_action_planner:add_action(
	create_exclamation_action_quest_completed(
		rural_market, 
		3, 
		"a brave hero slaying the wolves", 
		wolves_quest_tag))

-- market crier agent definitions
npc_agents[#npc_agents+1] = create_market_crier_agent(
	market_crier_action_planner, start_state.market_criers, urban_market)
npc_agents[#npc_agents+1] = create_market_crier_agent(
	market_crier_action_planner, start_state.market_criers, rural_market)



--[[
print("creating plan... ")

player_agent_table.goal_conditions = function(state_table)
	return 
	{
		state_table.current_agent_table.location == urban_castle
		--false
	}
end;

opt_plan = agent_plan(player_agent_table, start_state)

print("opt_plan tostring: ")
print(tostring(opt_plan))
--]]




local current_state = table.deepcopy(start_state)

print("Starting game loop...")
local quitting = false

local plan_time_measurements = {}

repeat
	local starvation_threshold = 20

	local current_location_table = agent_get_subtable(player_agent_table, current_state).location

	if is_valid_value(current_state.message) then
		print(current_state.message)
		current_state.message = nil
	end

	-- shuffle agents to randomize action order	
	shuffle_array(npc_agents)

	-- agents plan and make actions
	for _, current_agent in ipairs(npc_agents) do
		
		local agent_subtable = agent_get_subtable(current_agent, current_state)

		-- dynamic goal, not static -> has to be updated
		if (is_valid_value(agent_subtable.hunger)) then
			current_agent.goal_conditions = function(state_table)
			return 
			{
				-- less hungry than currently, not a specific value -> less planning ahead
				state_table.current_agent_table.hunger < current_state.current_agent_table.hunger
			}
			end
		end

		local startTime = os.clock()

		local opt_plan = agent_plan(current_agent, current_state)

		local elapsedTime = os.clock() - startTime
		plan_time_measurements[#plan_time_measurements + 1] = elapsedTime
		

		if (is_valid_value(opt_plan)) then
			local opt_action_state_table = opt_plan:first_action_state_table()
			if (is_valid_value(opt_action_state_table)) then
				-- only print actions of surrounding agents in same location
				if (agents_in_same_location(current_agent, player_agent_table, current_state)) then
					print(agent_get_subtable(current_agent, current_state).name .. " " .. opt_action_state_table.action.name)
				end
				current_state = opt_action_state_table.state
			else
				print("NO ACTION FOUND")
			end
				
		else
			print("NO PLAN FOUND FOR AGENT")
		end
		
		-- increase hunger
		if (is_valid_value(agent_subtable.hunger)) then
			agent_subtable.hunger = agent_subtable.hunger + 1
	
			if (agent_subtable.hunger >= starvation_threshold) then
				agent_subtable.dead = true
				print(agent_subtable.name .. " died of starvation")
			end
		end
	end

	
	---[[
	-- apply quest_world_changes
	do
		for quest_tag, change_function in pairs(quest_world_changes) do
			current_state = change_function(current_state)
		end
	end
	--]]
	

	-- remove dead agents 
	remove_if(npc_agents, function(current_agent)
		local agent_subtable = agent_get_subtable(current_agent, current_state)
		return get_table_flag(agent_subtable, "dead")
	end)

	local action_state_table = agent_possible_actions_and_states_table(player_agent_table, current_state)
	
	local action_selection_string = create_action_selection_string(action_state_table)

	local valid_input = index_strings(#action_state_table)
	-- allow quitting with 'q', added to 'valid_input'
	valid_input[#valid_input+1] = "q"

	-- print and read user input
	local input = read_and_return_valid_input(action_selection_string, valid_input)
	if (input == "q") then 
		print("q input")
		quitting = true
		do break end
	end

	-- use input to choose action and assign new state
	current_state = action_state_table[tonumber(input)].state

	
	local current_player_table = agent_get_subtable(player_agent_table, current_state)

	-- player quest budget
	current_player_table.quest_budget = current_player_table.quest_budget + 1

	-- player hunger
	do
		-- only manage hunger if agent has the value
		if (is_valid_value(current_player_table.hunger)) then
			-- increase hunger
			current_player_table.hunger = current_player_table.hunger + 1

			local starved = current_player_table.hunger >= starvation_threshold
			local warning = current_player_table.hunger >= starvation_threshold * 0.5

			if starved then
				quitting = true
				print("Player starved to death")
				do break end
			elseif warning then 
				print("Player is hungry")
			end
		end
	end

	-- http://www.lua.org/manual/5.1/manual.html#pdf-collectgarbage
	collectgarbage()

until quitting

print("quitting...")


print("average time for plan: " .. array_average(plan_time_measurements)
	.. " over " .. #plan_time_measurements ..  " executions")

table_to_json_file("last_state.json", current_state)


print("combination.lua end")