#pragma once

#include "action_lua.h"
#include "world_state_lua.h"


class a_star_node_lua
{
public:
	a_star_node_lua(const world_state_lua& current_state, const world_state_lua& previous_state, const action_lua& previous_action,
	            const float previous_cost, const sol::function& goal_conditions)
	{
		current_state_ = current_state;
		previous_state_ = previous_state;
		previous_action_ = previous_action;
		g_ = previous_cost + previous_action.cost();
		h_ = static_cast<float>(current_state.distance_to_goal(goal_conditions));
		f_ = g_ + h_;
	}

	float f() const
	{
		return f_;
	}

	float g() const
	{
		return g_;
	}

	const world_state_lua& current_state() const
	{
		return current_state_;
	}

	const world_state_lua& previous_state() const
	{
		return previous_state_;
	}

	const action_lua& previous_action() const
	{
		return previous_action_;
	}


private:
	// cost of the path from the start node to 'currentState'
	float g_;
	// heuristic that estimates the cost of the cheapest path from 'currentState' to the goal
	float h_;
	// g + h
	float f_;

	world_state_lua current_state_;
	world_state_lua previous_state_;
	action_lua previous_action_;
};
