#pragma once

#include <map>
#include <vector>

#include "string_id.h"

class action_lua
{
public:
	action_lua() = default;

	action_lua(const sol::table& table)
	{
		table_ = table;
		const std::string s = table["name"];
		name_ = s;
		cost_ = table["cost"];
		preconditions_ = sol::function(table["preconditions"]);
		effects_ = sol::function(table["effect"]);
	}

	const sol::function & effects() const
	{
		return effects_;
	}

	// uses 'string_id' for fast integer comparisons
	bool operator==(const action_lua& action) const
	{
		return name_ == action.name_;
	}

	bool operator!=(const action_lua& action) const
	{
		return !operator==(action);
	}

	std::string to_string() const
	{
		return name_.to_string();
	}

	const sol::function & preconditions() const
	{
		return preconditions_;
	}

	sol::table table() const
	{
		return table_;
	}

	float cost() const
	{
		return cost_;
	}

	friend std::ostream& operator<<(std::ostream& out, const action_lua& a)
	{
		return out << a.to_string();
	}

	// implementation needed for boost::graph properties
	friend std::istream& operator>>(std::istream& in, action_lua& a)
	{
		std::string string;
		auto & result = in >> string;
		a = action_lua();
		return result;
	}

private:
	string_id name_{};
	float cost_{};

	sol::function preconditions_;
	sol::function effects_;

	sol::table table_;

};


