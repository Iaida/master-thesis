print("deepcopy test.lua start")

LuaUtilities = require "LuaUtilities"


test_table = 
{
	test = true
}

function print_table(t)
	print("print_table called: ")

	print("original:")
	print(table.tostring(test_table))

	print("input:")
	print(table.tostring(t))
end

function change(t)
	t.changed = true
end


print("deepcopy test.lua end")