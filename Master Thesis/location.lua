print("location.lua start")

LuaUtilities = require "LuaUtilities"



function create_one_way_location_connection_action(source_location_name, target_location_name, action_cost)
	return 
	{
		name = "go from " .. source_location_name .. " to " .. target_location_name,
		cost = action_cost or 1,
		preconditions = function(state_table)
			return { state_table.location == source_location_name }
		end,
		effect = function(state_table)
			state_table.location = target_location_name
			return state_table
		end
	}
end

function add_one_way_location_connection_action(action_planner_instance, source_location_name, target_location_name, action_cost)
	action_planner_instance:add_action(create_one_way_location_connection_action(source_location_name, target_location_name, action_cost))
end

function add_two_way_location_connection_action(action_planner_instance, first_location_name, second_location_name, action_cost)
	add_one_way_location_connection_action(action_planner_instance, first_location_name, second_location_name, action_cost)
	add_one_way_location_connection_action(action_planner_instance, second_location_name, first_location_name, action_cost)
end



local ap = action_planner.new()

do
	local market = "market"
	local church = "church"
	local forge = "forge"
	local fields = "fields"
	local farm = "farm"
	local mill = "mill"
	local forest = "forest"
	local castle = "castle"

	add_two_way_location_connection_action(ap, market, church)
	add_two_way_location_connection_action(ap, market, forge)
	add_two_way_location_connection_action(ap, market, fields)
	add_two_way_location_connection_action(ap, fields, farm)
	add_two_way_location_connection_action(ap, fields, mill)
	add_two_way_location_connection_action(ap, fields, forest)
	add_two_way_location_connection_action(ap, forest, castle)
	--add_one_way_location_connection_action(ap, forest, castle)
end



start_state = 
{
	location = "market"
}



---[[
print("creating plan... ")

goal_conditions = function(state_table)
	return 
	{
		state_table.location == "castle"
	}
end;

opt_plan = a_star_plan.make_plan(ap, start_state, goal_conditions)
print("opt_plan tostring: ")
print(tostring(opt_plan))
--]]

print("Starting game loop...")

local quitting = false

current_state = table.deepcopy(start_state)
print(table.tostring(current_state))

repeat
	local action_state_table = ap:possible_actions_and_states_table(current_state)

	-- append action names 
	local message_to_concat = {"You are at " .. current_state.location}
	for i, action_state_pair in ipairs(action_state_table) do
		message_to_concat[#message_to_concat+1] = tostring(i) .. ": ".. action_state_pair.action.name
	end
	local message = table.concat(message_to_concat, "\n")

	local valid_input = index_strings(#action_state_table)
	-- allow quitting with 'q', added to 'valid_input'
	valid_input[#valid_input+1] = "q"

	-- print and read user input
	local input = read_and_return_valid_input(message, valid_input)
	if (input == "q") then 
		print("q input")
		quitting = true
		do break end
	end

	-- use input to choose action and assign new state
	local chosen_index = tonumber(input)
	current_state = action_state_table[chosen_index].state

until quitting

print("quitting...")

print("location.lua end")