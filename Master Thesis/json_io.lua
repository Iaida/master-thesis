JSON = require "JSON"


function string_from_file(filename)
  local f = io.open(filename, "rb")
  local content = f:read("*all")
  f:close()
  return content
end

function string_to_file(filename, string)
	local f = io.open(filename, "w")
	f:write(string)
	f:close()
end

function table_from_json_file(filename)
	local string = string_from_file(filename)
	return JSON:decode(string)
end

function table_to_json_file(filename, json_table)
	local string = JSON:encode_pretty(json_table)
	string_to_file(filename, string)
end

function pretty_print_json_table(json_table)
	print(JSON:encode_pretty(current_state))
end