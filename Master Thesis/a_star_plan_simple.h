#pragma once

#include <vector>
#include <iostream>

#include "action_simple.h"
#include "action_planner_simple.h"
#include "world_state_simple.h"
#include "a_star_node_simple.h"
#include "rng.h"

class a_star_plan_simple
{
public:

	// http://theory.stanford.edu/~amitp/GameProgramming/ImplementationNotes.html
	// https://www.redblobgames.com/pathfinding/a-star/introduction.html
	// https://www.redblobgames.com/pathfinding/a-star/implementation.html#cpp-astar
	// AI missionaries and cannibals
	// https://en.wikipedia.org/wiki/A*_search_algorithm

	static std::optional<a_star_plan_simple> make_plan(const action_planner_simple& action_planner, const world_state_simple& start, const world_state_simple& goal)
	{
		// The set of currently discovered nodes that are not evaluated yet.
		// Initially, only the start node is known.
		// not a priority queue because iteration is needed
		std::vector<a_star_node_simple> opened;
		// The set of nodes already evaluated
		std::vector<a_star_node_simple> closed;

		// add start node
		opened.emplace_back(a_star_node_simple(start, world_state_simple(), action_simple(), 0, goal));

		while (!opened.empty())
		{
			// find node with lowest f in 'opened'

			auto lowest_cost_node_it = std::min_element(std::cbegin(opened), std::cend(opened),
				[](const a_star_node_simple& a, const a_star_node_simple& b)
			{
				return a.f() < b.f();
			});
			a_star_node_simple current_node = *lowest_cost_node_it;
			opened.erase(lowest_cost_node_it);

			// check if state achieved 'goal'
			if (current_node.current_state().goal_reached(goal))
			{
				return a_star_plan_simple(action_planner, start, goal, current_node, closed);
			}

			// add 'currentNode' to closed
			closed.emplace_back(current_node);

			// iterate over neighbors of 'currentNode'
			for (const auto&[action, state] : action_planner.possible_actions_and_states(current_node.current_state()))
			{
				a_star_node_simple neighbor(state, current_node.current_state(), action, current_node.g(), goal);

				auto node_with_same_state_in_opened_it = std::find_if(std::cbegin(opened), std::cend(opened),
					[&neighbor](const a_star_node_simple& n)
				{
					return n.current_state() == neighbor.current_state();
				});

				auto node_with_same_state_in_closed_it = std::find_if(std::cbegin(closed), std::cend(closed),
					[&neighbor](const a_star_node_simple& n)
				{
					return n.current_state() == neighbor.current_state();
				});

				// if 'neighbor' state in 'opened' and cost g of 'neighbor' less than the node in 'opened':
				if (node_with_same_state_in_opened_it != std::cend(opened) &&
					neighbor.g() < node_with_same_state_in_opened_it->g())
				{
					// remove neighbor from 'opened', because new path is better
					opened.erase(node_with_same_state_in_opened_it);
					// neighbor is now no longer in 'opened', update 'nodeWithSameStateInOpenedIt' for following check
					node_with_same_state_in_opened_it = std::cend(opened);
				}

				// if 'neighbor' state in 'closed' and cost g of 'neighbor' less than the node in 'closed':
				if (node_with_same_state_in_closed_it != std::cend(closed) &&
					neighbor.g() < node_with_same_state_in_closed_it->g())
				{
					// remove neighbor from 'closed'
					closed.erase(node_with_same_state_in_closed_it);
					// neighbor is now no longer in 'closed', update 'nodeWithSameStateInClosedIt' for following check
					node_with_same_state_in_closed_it = std::cend(closed);
				}

				// if neighbor state not in OPEN and neighbor not in CLOSED:
				if (node_with_same_state_in_opened_it == std::cend(opened) &&
					node_with_same_state_in_closed_it == std::cend(closed))
				{
					// add 'neighbor' to 'opened'
					opened.emplace_back(neighbor);
				}
			}
		}

		// did not find a path
		return std::nullopt;
	}

	static std::optional<a_star_plan_simple> cheapest_plan(const action_planner_simple& action_planner, const world_state_simple& start, const std::vector<world_state_simple>& multiple_goals)
	{
		// 'world_state_simple' goals -> 'a_star_plan_simple' plans to each goal
		std::vector<a_star_plan_simple> plans;
		plans.reserve(multiple_goals.size());
		for (const world_state_simple & goal : multiple_goals)
		{
			auto opt_plan = make_plan(action_planner, start, goal);
			if (opt_plan)
				plans.emplace_back(*opt_plan);
		}

		// shuffle to get a random min_element result if cost is the same for multiple plans
		std::shuffle(std::begin(plans), std::end(plans), rng::engine);

		const auto result = std::min_element(std::cbegin(plans), std::cend(plans));

		if (result == std::cend(plans))
		{
			std::cout << "No valid plan found! \n";
			return std::nullopt;
		}
		return *result;
	}

	std::pair<action_simple, world_state_simple> first_action_state_pair() const
	{
		// constructed plan should always have at least 1 action and at least 2 world_states
		return std::make_pair(actions_[0], world_states_[1]);
	}

	std::string to_string() const
	{
		std::string result;
		result += "AStarPlan: cost: " + std::to_string(total_cost_) + "\n";

		for (size_t i = 0; i < world_states_.size(); i++)
		{
			result += "WorldState " + std::to_string(i) + ": " + world_states_[i].to_string() + "\n";
			if (i < actions_.size())
				result += "Action     " + std::to_string(i) + ": " + actions_[i].to_string() + "\n";
		}
		return result;
	}

	bool operator<(const a_star_plan_simple& rhs) const
	{
		return total_cost_ < rhs.total_cost_;
	}

private:

	// input values only for debugging purposes, not many plan instances atm, so no performance problem
	action_planner_simple action_planner_;
	world_state_simple start_;
	world_state_simple goal_;

	// TODO : symmetry, maybe pair<action_simple,state>?, include start
	// results
	std::vector<world_state_simple> world_states_; // 1 more element than actions, includes start
	std::vector<action_simple> actions_; // 1 less element than worldStates
	float total_cost_ = FLT_MAX;


	a_star_plan_simple(const action_planner_simple& action_planner, const world_state_simple& start, const world_state_simple& goal, 
		const a_star_node_simple& goal_node, const std::vector<a_star_node_simple>& closed) :
		action_planner_(action_planner), start_(start), goal_(goal)
	{
		reconstruct(goal_node, closed);
	}

	void reconstruct(const a_star_node_simple& goal_node, const std::vector<a_star_node_simple>& closed)
	{
		total_cost_ = goal_node.g();

		// start at 'goalNode'
		world_states_.emplace_back(goal_node.current_state());

		a_star_node_simple current_node = goal_node;

		while (true)
		{
			action_simple previous_action = current_node.previous_action();
			// check if start node reached, which has default values for previous action and world state
			if (previous_action == action_simple())
				break;

			actions_.emplace_back(previous_action);
			world_state_simple previous_state = current_node.previous_state();
			world_states_.emplace_back(previous_state);


			auto node_with_same_state_in_closed_it = std::find_if(std::cbegin(closed), std::cend(closed),
				[&previous_state](const a_star_node_simple& n)
			{
				return n.current_state() == previous_state;
			});

			if (node_with_same_state_in_closed_it != std::cend(closed))
				current_node = *node_with_same_state_in_closed_it;
			else
				break;
		}

		// reconstruction was done from goal to start
		// -> reverse containers
		std::reverse(world_states_.begin(), world_states_.end());
		std::reverse(actions_.begin(), actions_.end());
	}


	friend std::ostream& operator<<(std::ostream& stream, const a_star_plan_simple& plan)
	{
		return stream << plan.to_string();
	}


};

