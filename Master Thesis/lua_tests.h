#pragma once
#define SOL_CHECK_ARGUMENTS 1
#include "sol.hpp"


#include "custom_json.h"
#include <iostream>
#include <algorithm>

#include "json_to_lua_adapter.h"

#include "world_state_lua.h"
#include "action_planner_lua.h"
#include "a_star_plan_lua.h"

// TODO : redo tests

//
//inline void lua_test_conversions()
//{
//	custom_json default_villager;
//	read_json_from_file(default_villager, "villager default.json");
//
//	json_to_lua_adapter json_to_lua_adapter(default_villager);
//
//	sol::state lua;
//	lua.open_libraries(sol::lib::base, sol::lib::package);
//
//	std::cout << "Map Test \n";
//	const sol::table table_map = lua.create_table_with("bark", 50, "woof", 24);
//
//
//
//	//lua_wrapper.set(table_map);
//
//	write_json_to_file(default_villager, "lua result conversion map.json");
//
//
//	std::cout << "Vector Test \n";
//	lua.set("table_vec", sol::as_table(std::vector<int>{1, 2, 3, 4, 5}));
//	const sol::table table_vec = lua["table_vec"];
//
//
//	//lua_wrapper.set(table_vec);
//
//	write_json_to_file(default_villager, "lua result conversion vec.json");
//
//	std::cout << "lua_test_conversions finished \n";
//}





//inline void lua_test_action()
//{
//	custom_json default_villager;
//	read_json_from_file(default_villager, "villager default.json");
//
//
//	sol::state lua;
//	lua.open_libraries(sol::lib::base, sol::lib::package);
//
//
//
//	sol::usertype<json_to_lua_adapter> lua_json_type(
//		sol::meta_function::index, &json_to_lua_adapter::getter,
//		sol::meta_function::new_index, &json_to_lua_adapter::setter
//	);
//
//
//	const auto action = lua["action"];
//
//	lua.do_file("GoapTest.lua");
//	const std::string name = action["name"];
//	const float cost = action["cost"];
//	std::cout << "name " << name << " with cost " << cost << '\n';
//
//	const sol::protected_function condition_function = action["preconditions"];
//
//	sol::protected_function_result result = condition_function(json_to_lua_adapter(default_villager));
//
//	if (result.valid())
//	{
//		std::cout << "Call succeeded \n";
//		std::vector<bool> vec = result.get<std::vector<bool>>();
//		std::cout << "result : " << vec[0] << " " << vec[1] << '\n';
//	}
//
//	else
//	{
//		sol::error err = result;
//		std::cout << "Call failed, sol::error::what() is " << err.what() << '\n';
//	}
//
//	std::cout << "lua_test_table finished \n";
//}

