#pragma once

#include <map>
#include <vector>

#include "string_id.h"

class action_simple
{
public:
	action_simple() = default;

	action_simple(const string_id& name,
	       const float cost,
	       std::map<string_id, bool>&& preconditions,
	       std::map<string_id, bool>&& effects) :
		name_(name),
		cost_(cost),
		preconditions_(std::move(preconditions)),
		effects_(std::move(effects))
	{
	}

	action_simple(const string_id& name,
	       std::map<string_id, bool>&& preconditions,
	       std::map<string_id, bool>&& effects) :
		name_(name),
		cost_(1.0f), // default cost
		preconditions_(std::move(preconditions)),
		effects_(std::move(effects))
	{
	}

	std::vector<string_id> all_atoms() const
	{
		std::vector<string_id> result;
		for (const auto& [key, value] : preconditions_)
		{
			result.emplace_back(key);
		}
		for (const auto& [key, value] : effects_)
		{
			result.emplace_back(key);
		}
		return result;
	}

	const std::map<string_id, bool>& effects() const
	{
		return effects_;
	}

	bool operator==(const action_simple& action) const
	{
		return name_ == action.name_;
	}

	bool operator!=(const action_simple& action) const
	{
		return !operator==(action);
	}

	std::string to_string() const
	{
		return name_.to_string();
	}

	const std::map<string_id, bool>& preconditions() const
	{
		return preconditions_;
	}

	float cost() const
	{
		return cost_;
	}

	friend std::ostream& operator<<(std::ostream& out, const action_simple& a)
	{
		return out << a.to_string();
	}

	// implementation needed for boost::graph properties
	friend std::istream& operator>>(std::istream& in, action_simple& a)
	{
		std::string string;
		auto & result = in >> string;
		a = action_simple();
		return result;
	}

private:
	string_id name_{};
	float cost_{};

	std::map<string_id, bool> preconditions_;
	std::map<string_id, bool> effects_;
};


