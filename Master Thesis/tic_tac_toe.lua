print("tic_tac_toe.lua start")

print(_VERSION)

LuaUtilities = require "LuaUtilities"

local field_value = 
{
	free = 0,
	circle = 1,
	cross = 2
}

local start_state = 
{
	fields = 
	{
		field_value.free, field_value.free,	field_value.free,
		field_value.free, field_value.free,	field_value.free,
		field_value.free, field_value.free,	field_value.free,
	}
}

function field_value_to_string(value)
	if value == 2 then
		return "X"
	elseif value == 1 then
		return "O"
	else
		return "-"
	end
end

function print_fields(fields)
	print("Current State:")
	print(field_value_to_string(fields[7]) .. 
		field_value_to_string(fields[8]) .. 
		field_value_to_string(fields[9]))
	print(field_value_to_string(fields[4]) .. 
		field_value_to_string(fields[5]) .. 
		field_value_to_string(fields[6]))
	print(field_value_to_string(fields[1]) .. 
		field_value_to_string(fields[2]) .. 
		field_value_to_string(fields[3]))
end

function check_game_over(current_state)
	if (get_table_flag(current_state, "winner")) then
		print("Winner is player " .. current_state.winner)
		return true
	end

	-- check for draw, meaning no empty field left
	for _, v in ipairs(current_state.fields) do 
		if v == field_value.free then
			return false
		end
	end
	print("No winner, draw")
	return true
end


function check_for_winning_combination(field, player, positionA, positionB, positionC)
	return field[positionA] == field_value[player] and
		field[positionB] == field_value[player] and
		field[positionC] == field_value[player]
end

function check_for_winning_position(field, player, position)
	-- horizontal, vertical, diagonal checks
	return check_for_winning_combination(field, player, 1, 2, 3) or
		check_for_winning_combination(field, player, 4, 5, 6) or
		check_for_winning_combination(field, player, 7, 8, 9) or
		check_for_winning_combination(field, player, 1, 4, 7) or
		check_for_winning_combination(field, player, 2, 5, 8) or
		check_for_winning_combination(field, player, 3, 6, 9) or
		check_for_winning_combination(field, player, 1, 5, 9) or
		check_for_winning_combination(field, player, 3, 5, 7)
end


function create_action(name, preconditions, effect, cost)
	return
	{
		name = name,
		cost = cost or 1.0,
		preconditions = preconditions,
		effect = effect
	}
end


function create_player_action(player, position)
	local cost = 1.0
	-- middle: part of 4 goals
	-- corners: part of 3 goals
	-- sides: part of 2 goals
	if position == 5 then
		cost = 1.0 / 4.0
	elseif (position % 2) == 1 then
		cost = 1.0 / 3.0
	else 
		cost = 1.0 / 2.0
	end

	return create_action(
		"set" .. position .. player,
		function(state_table)
			return 
			{
				state_table.fields[position] == field_value.free
			}
		end,
		function(state_table)
			state_table.fields[position] = field_value[player]
			if check_for_winning_position(state_table.fields, player, position) then
				state_table.winner = player
			end
			return state_table
		end,
		cost
		)
	
end



function create_actions_for_player(player)
	result = {}
	for i = 1, 9 do
		result[#result+1] = create_player_action(player, i)
	end
	return result
end


local cross_ap = action_planner.new()
cross_ap:add_action_array(create_actions_for_player("cross"))

local circle_ap = action_planner.new()
circle_ap:add_action_array(create_actions_for_player("circle"))

---[[
print("creating plan... ")

goal_conditions = function(state_table)
	return 
	{
		state_table.winner == "circle"
	}
end;

opt_plan = a_star_plan.make_plan(circle_ap, start_state, goal_conditions)
print("opt_plan tostring: ")
print(tostring(opt_plan))
--]]

local current_state = table.deepcopy(start_state)

print("Starting game loop...")
local quitting = false


local player_controlled = true

repeat

	-- circle is ai controlled
	local opt_plan = a_star_plan.make_plan(
		circle_ap, 
		current_state, 
		function(state_table) return { state_table.winner == "circle" } end
	)
	if (is_valid_value(opt_plan)) then
		local opt_action_state_table = opt_plan:first_action_state_table()
		if (is_valid_value(opt_action_state_table)) then
			-- assign new state
			current_state = opt_action_state_table.state
		else
			print("NO ACTION FOUND")
		end
				
	else
		print("NO PLAN FOUND FOR AGENT")
	end

	
	print_fields(current_state.fields)
	if check_game_over(current_state) then
		quitting = true
		break
	end
	
	-- cross controlled by player 
	
	local ast = cross_ap:possible_actions_and_states_table(current_state)

	-- append action names to 'current_state.message'
	local message_to_concat = {current_state.message}
	for i, action_state_pair in ipairs(ast) do
		message_to_concat[#message_to_concat+1] = tostring(i) .. ": ".. action_state_pair.action.name
	end
	local message = table.concat(message_to_concat, "\n")

	local valid_input = index_strings(#ast)
	-- allow quitting as valid input
	valid_input[#valid_input+1] = "q"

	-- print and read user input
	local input = read_and_return_valid_input(message, valid_input)
	if (input == "q") then 
		print("q input")
		quitting = true
		do break end
	end

	-- use input to choose action and assign new state
	local chosen_index = tonumber(input)
	current_state = ast[chosen_index].state
	
	
	print_fields(current_state.fields)
	if check_game_over(current_state) then
		quitting = true
		break
	end
	

until quitting


print("quitting...")




print("tic_tac_toe.lua end")