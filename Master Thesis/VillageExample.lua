print("VillageExample.lua start")

LuaUtilities = require "LuaUtilities"



function random_hiragana_name()
	local hiragana =
	{
		"a",  "i",   "u",   "e",  "o",
		"ka", "ki",  "ku",  "ke", "ko",
		"ga", "gi",  "gu",  "ge", "go",
		"sa", "shi", "su",  "se", "so",
		"za", "ji",  "zu",  "ze", "zo",
		"ta", "chi", "tsu", "te", "to",
		"da", "ji",  "zu",  "de", "do",
		"na", "ni",  "nu",  "ne", "no",
		"ha", "hi",  "hu",  "he", "ho",
		"ba", "bi",  "bu",  "be", "bo",
		"pa", "pi",  "pu",  "pe", "po",
		"ma", "mi",  "mu",  "me", "mo",
		"ya",        "yu",        "yo",
		"ra", "ri",  "ru",  "re", "ro",
		"wa",                     "o",
		"n"
	}

	
	local character_array = {}
	local n = math.random(2, 3)
	for i=1, n do
		local random_index = math.random(#hiragana)
		table.insert(character_array, hiragana[random_index])
	end

	-- https://stackoverflow.com/questions/1405583/concatenation-of-strings-in-lua
	return table.concat(character_array)
end



init_random()

local default_villager = table_from_json_file("villager default.json")
local worldstate = table_from_json_file("worldstate default.json")

local start_food = 10

worldstate["village"]["food"] = start_food


local villager_array = worldstate["village"]["villagers"]
local num_villagers = 10
for i=1, num_villagers do
	-- modify default 'villager'
	local new_villager = table.deepcopy(default_villager);
	new_villager["name"] = random_hiragana_name()
	new_villager["hunger"] = math.random(0, 1)
	table.insert(villager_array, new_villager)
end

print("Number Villagers: " .. #villager_array)
print("First Villager name: " .. villager_array[1]["name"])

table_to_json_file("worldstate lua day0.json", worldstate)


local days_of_simulation = 30
for d=1, days_of_simulation do
	print(#villager_array .. " Villagers left")
	if #villager_array == 0 then
		break
	end

	

	-- sort by hunger descending
	table.sort(villager_array, function(a, b) return a["hunger"] > b["hunger"] end)

	print(#villager_array .. " Villagers left")

	for _, v in ipairs(villager_array) do
		print(v["name"] .. " simulation")

		-- new day, increase hunger
		v["hunger"] = v["hunger"] + 1

		-- search for food
		-- randomness as placeholder, can be replaced with GOAP
		if (random_bool(0.5)) then
			-- found food for themselves
			print("found food")
			v["hunger"] = v["hunger"] - 1

			if (random_bool(0.5)) then
				-- found enough to store leftovers
				print("Villager " .. v["name"] .. " found additional food")
				worldstate["village"]["food"] = worldstate["village"]["food"] + 1
			end
		else
			print("did not find food")

			-- check village storage
			if worldstate["village"]["food"] > 0 then
				print("eats food from village storage")
				worldstate["village"]["food"] = worldstate["village"]["food"] - 1
				v["hunger"] = v["hunger"] - 1
			else
				print("no food left in village storage")
			end
		end
	end

	
	-- remove villagers who were starving for 3 days
	remove_if(villager_array, 
		function(v)
			result = v["hunger"] >= 3
			if (result) then
				print("Villager " .. v["name"] .. " died of starvation " .. " with hunger " ..  v["hunger"])
			end
			return result
		end)


	-- day over
	print("day " .. worldstate["day"] .. " over")
	worldstate["day"] = worldstate["day"] + 1

end

table_to_json_file("worldstate lua day30.json", worldstate)

print("VillageExample.lua end")
