#pragma once
#include "tic_tac_toe.h"


void print_tic_tac_toe_world_state(const world_state_simple & state)
{
	const std::map<string_id, bool> & state_map = state.states();
	const auto num_pad_to_symbol = [&state_map](const char num_pad_character) -> char
	{
		const auto atom = std::find_if(std::cbegin(state_map), std::cend(state_map), 
			[&num_pad_character](const std::pair<string_id, bool> & atom) -> bool
		{
			return atom.second && atom.first.to_string()[0] == num_pad_character;
		});
		return atom->first.to_string()[1];
	};

	std::cout << num_pad_to_symbol('7') << num_pad_to_symbol('8') << num_pad_to_symbol('9') << '\n';
	std::cout << num_pad_to_symbol('4') << num_pad_to_symbol('5') << num_pad_to_symbol('6') << '\n';
	std::cout << num_pad_to_symbol('1') << num_pad_to_symbol('2') << num_pad_to_symbol('3') << '\n';
	
}

action_planner_simple create_tic_tac_toe_action_planner(const tic_tac_toe::field_value target_value)
{
	action_planner_simple action_planner;
	for (auto & action : tic_tac_toe::possible_actions(target_value))
	{
		action_planner.add_action(std::move(action));
	}
	return action_planner;

}

world_state_simple create_tic_tac_toe_world_state_from_atoms(const std::vector<std::string> & atoms)
{
	std::map<string_id, bool> states;
	for (const auto & atom : atoms)
	{
		states[atom] = true;
	}
	return states;
}

std::vector<world_state_simple> create_tic_tac_toe_goal_states(const std::vector<std::vector<std::string>> & goals)
{
	std::vector<world_state_simple> result;
	result.reserve(goals.size());
	for (const auto & goal_state_atoms : goals)
	{
		result.emplace_back(create_tic_tac_toe_world_state_from_atoms(goal_state_atoms));
	}
	return result;
}

std::pair<action_simple, world_state_simple> chosen_action_state_pair(const action_planner_simple& action_planner, const world_state_simple& current_state)
{
	const auto action_state_pairs = action_planner.possible_actions_and_states(current_state);


	while (true)
	{
		std::cout << "Choose a position by using the numblock keys 1-9 \n";
		std::string raw_input;
		std::cin >> raw_input;
		size_t position;
		const auto parse_result = std::from_chars(raw_input.c_str(),
			raw_input.c_str() + raw_input.size(),
			position);
		if (parse_result.ec == std::errc::invalid_argument || position < 1 || position > 9)
		{
			std::cout << "Input was not a valid position \n";
		}
		else
		{
			auto result = std::find_if(std::cbegin(action_state_pairs), std::cend(action_state_pairs),
				[&raw_input](const std::pair<action_simple, world_state_simple> & action_state_pair)
			{
				return action_state_pair.first.to_string()[0] == raw_input[0];
			});

			if (result == std::cend(action_state_pairs))
			{
				std::cout << "Input did not fit a possible action \n";
			}
			else
			{
				return *result;
			}
		}
	}
}


void double_ai_tic_tac_toe_test()
{
	const auto start_state = create_tic_tac_toe_world_state_from_atoms(tic_tac_toe::start_atoms());

	auto cross_action_planner = create_tic_tac_toe_action_planner(tic_tac_toe::field_value::cross);
	auto circle_action_planner = create_tic_tac_toe_action_planner(tic_tac_toe::field_value::circle);

	auto cross_goals = create_tic_tac_toe_goal_states(tic_tac_toe::goals(tic_tac_toe::field_value::cross));
	auto circle_goals = create_tic_tac_toe_goal_states(tic_tac_toe::goals(tic_tac_toe::field_value::circle));

	world_state_simple current_state = start_state;
	std::cout << "Start State: \n";
	print_tic_tac_toe_world_state(current_state);

	size_t step = 1;
	bool cross = rng::coin_flip_bool();
	while (true)
	{
		if (step > 9)
		{
			std::cout << "Maximum steps reached, draw \n";
			break;
		}

		action_planner_simple* current_planner = cross ? &cross_action_planner : &circle_action_planner;
		std::vector<world_state_simple>* current_goals = cross ? &cross_goals : &circle_goals;

		auto opt_cheapest_plan = a_star_plan_simple::cheapest_plan(*current_planner, current_state, *current_goals);
		if (!opt_cheapest_plan)
		{
			std::cout << "No plan found to reach goal \n";
			break;
		}

		const auto[first_action, first_state] = opt_cheapest_plan->first_action_state_pair();
		std::cout << "Current Player: " << (cross ? "cross" : "circle") << '\n';
		std::cout << "Action: " << first_action << '\n';
		current_state = first_state;
		std::cout << "New State: \n";
		print_tic_tac_toe_world_state(current_state);

		if (current_state.any_goal_reached(*current_goals))
		{
			std::cout << "Goal reached, " << (cross ? "cross" : "circle") << " won \n";
			break;
		}
		step++;
		cross = !cross;
	}
	std::cout << "double_ai_tic_tac_toe_test finished \n";
}





void player_vs_ai_tic_tac_toe_test()
{
	const auto start_state = create_tic_tac_toe_world_state_from_atoms(tic_tac_toe::start_atoms());

	const auto cross_action_planner = create_tic_tac_toe_action_planner(tic_tac_toe::field_value::cross);
	const auto circle_action_planner = create_tic_tac_toe_action_planner(tic_tac_toe::field_value::circle);

	auto cross_goals = create_tic_tac_toe_goal_states(tic_tac_toe::goals(tic_tac_toe::field_value::cross));
	auto circle_goals = create_tic_tac_toe_goal_states(tic_tac_toe::goals(tic_tac_toe::field_value::circle));

	world_state_simple current_state = start_state;
	std::cout << "Start State: \n";
	print_tic_tac_toe_world_state(current_state);

	size_t step = 1;
	bool cross = rng::coin_flip_bool();
	while (true)
	{
		if (step > 9)
		{
			std::cout << "Maximum steps reached, draw \n";
			break;
		}

		std::vector<world_state_simple>* current_goals = cross ? &cross_goals : &circle_goals;

		std::cout << "Current Player: " << (cross ? "cross" : "circle") << '\n';
		if (cross)
		{
			auto opt_cheapest_plan = a_star_plan_simple::cheapest_plan(cross_action_planner, current_state, *current_goals);

			const auto[first_action, first_state] = opt_cheapest_plan->first_action_state_pair();

			std::cout << "Action: " << first_action << '\n';

			current_state = first_state;
			
		}
		else
		{
			const auto[chosen_action, chosen_state] = chosen_action_state_pair(circle_action_planner, current_state);

			std::cout << "Action: " << chosen_action << '\n';

			current_state = chosen_state;
		}

		std::cout << "New State: \n";
		print_tic_tac_toe_world_state(current_state);


		if (current_state.any_goal_reached(*current_goals))
		{
			std::cout << "Goal reached, " << (cross ? "cross" : "circle") << " won \n";
			break;
		}
		step++;
		cross = !cross;
	}
	std::cout << "player_vs_ai_tic_tac_toe_test finished \n";
	std::cin.get();
}