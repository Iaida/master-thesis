dc = require "deepcopy"
json_io = require "json_io"
table_equality = require "table_equality"
table_printing = require "table_printing"


function random_bool(true_probability)
	return math.random() < true_probability
end

-- https://stackoverflow.com/questions/18199844/lua-math-random-not-working/18199908#18199908
function init_random()
	math.randomseed(os.time())
	math.random(); math.random(); math.random()
end

-- https://stackoverflow.com/questions/12394841/safely-remove-items-from-an-array-table-while-iterating/12397742#12397742
-- Moves objects for which predicate returns 'true' to the end of the array and assigns them the value 'nil'.
-- arrays with nil values at the end are automatically shortened by lua.
function remove_if(input_array, predicate)
	local n=#input_array

	for i=1, n do
		if predicate(input_array[i]) then
			input_array[i] = nil
		end
	end

	local j=0
	for i=1, n do
        if input_array[i] ~= nil then
            j = j + 1
            input_array[j] = input_array[i]
        end
	end

	for i=j+1, n do
		input_array[i] = nil
	end
end

function any(input_array, predicate)
	for _, value in ipairs(input_array) do
		if predicate(value) then
			return true
		end
	end
	return false
end


function read_and_return_valid_input(message, valid_input_strings)
	local input
	repeat
	   io.write(message .. "\n")
	   io.flush()
	   input=io.read()
	until any(valid_input_strings, function(value) return value == input end)
	return input
end



-- returns array of indices as strings
function index_strings(size)
	local result_array = {}
	for i=1, size do
		table.insert(result_array, tostring(i))	
	end
	return result_array
end

function random_array_element(array)
	return array[math.random(#array)]
end

-- https://stackoverflow.com/questions/2988246/choose-a-random-item-from-a-table/37468712#37468712
function random_table_element(table)
	-- iterate over whole table to get all keys
	local keyset = {}
	for k in pairs(myTable) do
		table.insert(keyset, k)
	end
	-- now you can reliably return a random key
	random_elem = myTable[keyset[math.random(#keyset)]]
end



-- https://stackoverflow.com/questions/17119804/lua-array-shuffle-not-working/17120745#17120745
function shuffle_array(array)
	local function swap_array_elements(array, index1, index2)
		array[index1], array[index2] = array[index2], array[index1]
	end

	local counter = #array
    while counter > 1 do
        local index = math.random(counter)
		swap_array_elements(array, index, counter)
        counter = counter - 1
    end
end

function random_bool()
	return math.random(0,1) == 1
end

function table_length(table)
	local count = 0
	for _ in pairs(table) do 
		count = count + 1 
	end
	return count
end


-- also known as ternary operator
-- careful with boolean values for a and b: http://lua-users.org/wiki/TernaryOperator
function conditional_op(condition, a, b)
	return condition and a or b
end

function is_valid_value(value)
	return value ~= nil
end


function get_table_flag(table, flag_name)
	return is_valid_value(table[flag_name])
end

function set_table_flag(table, flag_name, active)
	table[flag_name] = conditional_op(active, true, nil)
end

function array_average(input_array)
	local acc = 0
	for _, value in ipairs(input_array) do
		acc = acc + value
	end
	return acc / #input_array
end
