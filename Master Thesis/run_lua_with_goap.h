#pragma once
#include <iostream>

#define SOL_CHECK_ARGUMENTS 1
#include "sol.hpp"


#include "world_state_lua.h"
#include "action_planner_lua.h"
#include "a_star_plan_lua.h"
#include "graphviz_lua.h"

inline void run_lua_file_with_goap(const std::string & filename)
{
	sol::state lua;
	world_state_lua::state_ptr = lua.lua_state();

	lua.open_libraries(
		sol::lib::base,
		sol::lib::package,
		sol::lib::table,
		sol::lib::math,
		sol::lib::string,
		sol::lib::io,
		sol::lib::os,
		sol::lib::debug
	);

	lua.new_usertype<action_planner_lua>("action_planner",
		"new", sol::constructors<
			action_planner_lua(),
			action_planner_lua(const action_planner_lua &)
		>(),
		"add_action", sol::resolve<bool(const sol::table &)>(&action_planner_lua::add_action),
		"add_action_array", &action_planner_lua::add_action_array,
		"possible_actions_and_states_table", &action_planner_lua::possible_actions_and_states_table
		);

	lua.new_usertype<a_star_plan_lua>("a_star_plan",
		"new", sol::no_constructor,
		"make_plan", a_star_plan_lua::make_plan,
		"first_action_state_table", &a_star_plan_lua::first_action_state_table
		);


	lua.new_usertype<graphviz_lua>("graphviz",
		sol::default_constructor,
		"write_graph", &graphviz_lua::write_graph,
		"add_vertex_to_graph", &graphviz_lua::add_vertex_to_graph,
		"add_edge_to_graph", &graphviz_lua::add_edge_to_graph		
		);

	sol::protected_function_result result = lua.do_file(filename);

	if (result.valid())
	{
		std::cout << "Call succeeded \n";
	}
	else
	{
		sol::error err = result;
		std::cout << "Call failed, sol::error::what() is " << err.what() << '\n';
	}

	/*const sol::table test_table = lua["test_table"];
	const sol::protected_function print_test = lua["print_table"];
	const sol::protected_function change = lua["change"];

	auto p1_res = print_test(test_table);
	const sol::table test_table_copy = callable_lua_functions::table_deepcopy(lua, test_table);
	auto change_res = change(test_table_copy);
	auto p2_res = print_test(test_table_copy);*/
	


	std::cout << "run_lua_file_with_goap finished \n";
}
