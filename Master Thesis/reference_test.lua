print("reference_test.lua start")

LuaUtilities = require "LuaUtilities"
--[[


-- https://stackoverflow.com/questions/11218330/how-to-assign-lua-variable-by-reference

a = { value = "Testing 1,2,3" }
b = a

-- b and a now refer to the same table
print(a.value) -- Testing 1,2,3
print(b.value) -- Testing 1,2,3

a = { value = "Duck" }
--a.value = "Duck"

-- a now refers to a different table; b is unaffected
print(a.value) -- Duck
print(b.value) -- Testing 1,2,3

print(a == b)
--]]

a_table = { name = "a" }
b_table = { name = "b" }
c_table = { name = "c" }

original = 
{ 
	subtable = a_table
}
original.ref = original.subtable

print(table.tostring(original))
original.ref = b_table

print(table.tostring(original))

copy = table.deepcopy(original)
print(table.tostring(copy))

copy.ref = c_table
print(table.tostring(copy))

print("reference_test.lua end")