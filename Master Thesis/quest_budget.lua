
-- TODO: remove redundant deepcopies in effects after changes 
-- TODO: actions with better preconditions, not added at runtime

print("quest_budget.lua start")

LuaUtilities = require "LuaUtilities"

function create_quest_start_action(quest_info_table)
	return
	{
		name = "Talk to " .. quest_info_table.quest_giver_name,
		cost = 1.0,
		preconditions = function(state_table)
			return 
			{ 
				state_table.location == quest_info_table.location,
				not get_table_flag(state_table, "unlocked_quest_" .. quest_info_table.quest_giver_name)
			}
		end,
		effect = function(state_table)
			local result = table.deepcopy(state_table)
			-- add unlock flag for further quest progress
			result["unlocked_quest_" .. quest_info_table.quest_giver_name] = true
			-- decrease quest_budget to improve pacing, budget only removed if action was chosen and quest unlocked
			result.quest_budget = result.quest_budget - quest_info_table.quest_budget
			result.message = quest_info_table.message
			return result
		end
	}
end

function add_random_quest_start_action(quest_array, current_quest_budget, current_action_planner)
	local quest_array_filtered = table.deepcopy(quest_array)
	-- remove quests which are not choosable with the current quest_budget
	remove_if(quest_array_filtered, function(quest) return quest.quest_budget > current_quest_budget end)
	if #quest_array_filtered ~= 0 then
		local random_quest_info_table = random_array_element(quest_array_filtered)
		local random_quest_start_action = create_quest_start_action(random_quest_info_table)
		current_action_planner:add_action(random_quest_start_action)
	end
end


local quest_array = 
{
	{
		quest_giver_name = "Village Chief",
		message = "Wolves are attacking our farms!",
		quest_budget = 1,
		location = "townhall"
	},
	{
		quest_giver_name = "Blacksmith",
		message = "Bandits are plundering my shipments!",
		quest_budget = 3,
		location = "marketplace"
	},
	--...
}


local starting_world_state = 
{
	quest_budget = 0,
	location = "marketplace",
	message = "In marketplace"
}

local ap = action_planner.new(
{
	{
		name = "Move to marketplace",
		cost = 1.0,
		preconditions = function(state_table)
			return { state_table.location == "townhall" }
		end,
		effect = function(state_table)
			local result = table.deepcopy(state_table)
			result.location = "marketplace"
			result.quest_budget = result.quest_budget + 1
			result.message = "In marketplace"
			return result
		end
	},
	{
		name = "Move to townhall",
		cost = 1.0,
		preconditions = function(state_table)
			return { state_table.location == "marketplace" }
		end,
		effect = function(state_table)
			local result = table.deepcopy(state_table)
			result.location = "townhall"
			result.quest_budget = result.quest_budget + 1
			result.message = "In townhall"
			return result
		end
	}
})



local quitting = false

local current_state = starting_world_state

repeat

	add_random_quest_start_action(quest_array, current_state.quest_budget, ap)

	local ast = ap:possible_actions_and_states_table(current_state)

	-- append action names to 'current_state.message'
	local message_to_concat = {current_state.message}
	for i, action_state_pair in ipairs(ast) do
		message_to_concat[#message_to_concat+1] = tostring(i) .. ": ".. action_state_pair.action.name
	end
	local message = table.concat(message_to_concat, "\n")

	local valid_input = index_strings(#ast)
	-- allow quitting as valid input
	valid_input[#valid_input+1] = "q"

	-- print and read user input
	local input = read_and_return_valid_input(message, valid_input)
	if (input == "q") then 
		print("q input")
		quitting = true
		do break end
	end

	-- use input to choose action and assign new state
	local chosen_index = tonumber(input)
	current_state = ast[chosen_index].state

until quitting

print("quitting...")


print("quest_budget.lua end")