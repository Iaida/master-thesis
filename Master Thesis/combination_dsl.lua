LuaUtilities = require "LuaUtilities"

-- TODO agent vs table in state


function update_current_agent(current_state, agent_table)
	current_state.current_agent_table = agent_table.subtable_function(current_state)
end

function create_agent(agent_action_planner, subtable_function, goal_conditions)
	return 
	{
		subtable_function = subtable_function,
		agent_action_planner = agent_action_planner,
		goal_conditions = goal_conditions or nil
	}
end

-- can lead to infinite loop, when goal cannot be achieved, but actions (such as moving) are still possible and mostly same states do not get detected
function agent_plan(agent_table, current_state)
	update_current_agent(current_state, agent_table)
	return a_star_plan.make_plan(agent_table.agent_action_planner, current_state, agent_table.goal_conditions)
end

function agent_plan_first_action_world_state_table(agent_table, current_state)
	opt_plan = agent_plan(agent_table, current_state)
	if (not is_valid_value(opt_plan)) then
		return nil
	end

	return opt_plan:first_action_state_table()
end

-- returns array of {action, state} tables
function agent_possible_actions_and_states_table(agent_table, current_state)
	update_current_agent(current_state, agent_table)
	return agent_table.agent_action_planner:possible_actions_and_states_table(current_state)
end

function agent_set_location(agent_table, current_state, location_table)
	agent_table.subtable_function(current_state).location = location_table
end

function agent_get_subtable(agent_table, current_state)
	return agent_table.subtable_function(current_state)
end


function same_location(location_a, location_b)
	return isEqual(location_a, location_b)
end

function agents_in_same_location(agent_table_a, agent_table_b, current_state)
	return same_location(
		agent_get_subtable(agent_table_a, current_state).location, 
		agent_get_subtable(agent_table_b, current_state).location)
end


function add_village(village_name, villages_table)
	local new_village = 
	{
		name = village_name,
		village_parts = {},
		villagers = {},
		reputation = 0
	}
	villages_table[village_name] = new_village
	return new_village
end

function add_village_part(village_table, village_part_name)
	local new_village_part = 
	{
		name = village_part_name,
		village_name = village_table.name
	}
	village_table.village_parts[#village_table.village_parts+1] = new_village_part
	return new_village_part
end

function create_one_way_location_connection_action(source_location_table, target_location_table, action_cost)
	return 
	{
		name = "goes from " .. source_location_table.name .. " in " ..  source_location_table.village_name .. 
			" to " .. target_location_table.name .. " in " ..  target_location_table.village_name,
		cost = action_cost or 1,
		preconditions = function(state_table)
			-- prefer value equality over reference to avoid errors
			return { isEqual(state_table.current_agent_table.location, source_location_table) }
		end,
		effect = function(state_table)
			state_table.current_agent_table.location = target_location_table
			if isEqual(state_table.current_agent_table, state_table.player) then
				state_table.message = "You are now at " .. target_location_table.name .. " in " .. target_location_table.village_name
			end
			return state_table
		end
	}
end

function add_one_way_location_connection_action(action_array, source_location_table, target_location_table, action_cost)
	action_array[#action_array+1] = create_one_way_location_connection_action(source_location_table, target_location_table, action_cost)
end

function add_two_way_location_connection_action(action_array, first_location_table, second_location_table, action_cost)
	add_one_way_location_connection_action(action_array, first_location_table, second_location_table, action_cost)
	add_one_way_location_connection_action(action_array, second_location_table, first_location_table, action_cost)
end


function random_hiragana_name()
	local hiragana =
	{
		"a",  "i",   "u",   "e",  "o",
		"ka", "ki",  "ku",  "ke", "ko",
		"ga", "gi",  "gu",  "ge", "go",
		"sa", "shi", "su",  "se", "so",
		"za", "ji",  "zu",  "ze", "zo",
		"ta", "chi", "tsu", "te", "to",
		"da", "ji",  "zu",  "de", "do",
		"na", "ni",  "nu",  "ne", "no",
		"ha", "hi",  "hu",  "he", "ho",
		"ba", "bi",  "bu",  "be", "bo",
		"pa", "pi",  "pu",  "pe", "po",
		"ma", "mi",  "mu",  "me", "mo",
		"ya",        "yu",        "yo",
		"ra", "ri",  "ru",  "re", "ro",
		"wa",                     "o",
		"n"
	}

	
	local character_array = {}
	local n = math.random(2, 3)
	for i=1, n do
		local random_index = math.random(#hiragana)
		table.insert(character_array, hiragana[random_index])
	end

	-- https://stackoverflow.com/questions/1405583/concatenation-of-strings-in-lua
	return table.concat(character_array)
end

function create_villager_agent(villager_action_planner, village_table, start_village_part)
	local villager_name = random_hiragana_name()
	local new_villager = 
	{
		name = villager_name,
		home_village = village_table.name,
		location = start_village_part,
		hunger = math.random(1, 3),
		currency = math.random(0,3),
	}
	
	village_table.villagers[villager_name] = new_villager
	return create_agent(villager_action_planner, function(state_table)
		return state_table.villages[village_table.name].villagers[villager_name]
	end)
end

function create_market_crier_agent(market_crier_action_planner, market_criers_table, start_village_part)
	local market_crier_name = random_hiragana_name()
	local new_market_crier = 
	{
		name = market_crier_name,
		location = start_village_part,
		hunger = math.random(1, 3),
		currency = math.random(0,3),
	}
	
	market_criers_table[market_crier_name] = new_market_crier
	return create_agent(
		market_crier_action_planner, 
		function(state_table)
			return state_table.market_criers[market_crier_name]
		end)
end




function create_action(name, preconditions, effect, cost)
	return
	{
		name = name,
		cost = cost or 1.0,
		preconditions = preconditions,
		effect = effect
	}
end

function create_job_action(location_table, currency_gain, cost)
	return create_action(
		"works at " .. location_table.name,
		function(state_table)
			return 
			{
				isEqual(state_table.current_agent_table.location, location_table)				
			}
		end,
		function(state_table)
			state_table.current_agent_table.currency = state_table.current_agent_table.currency + currency_gain
			return state_table
		end, 
		cost
	)
end

function create_job_action_planner(villager_action_planner, job_action)
	local villager_action_planner_copy = action_planner.new(villager_action_planner)
	villager_action_planner_copy:add_action(job_action)
	return villager_action_planner_copy
end

function create_exclamation_action(location_table, currency_gain)
	return create_action(
	"exclaims at " .. location_table.name  .. " in " .. location_table.village_name,
	function(state_table)
		return 
		{
			isEqual(state_table.current_agent_table.location, location_table)
			--table_length(state_table.player.quests_completed) == 0
		}
	end,
	function(state_table)
		state_table.current_agent_table.currency = state_table.current_agent_table.currency + currency_gain
		return state_table
	end,
	-- higher cost to prefer other exclamation actions
	2
	)
end

function create_exclamation_action_quest_completed(location_table, currency_gain, description_string, quest_tag)
	return create_action(
	"exclaims at " .. location_table.name  .. " in " .. location_table.village_name .. " about " .. description_string,
	function(state_table)
		return 
		{
			isEqual(state_table.current_agent_table.location, location_table),
			get_table_flag(state_table.player.quests_completed, quest_tag)
		}
	end,
	function(state_table)
		state_table.current_agent_table.currency = state_table.current_agent_table.currency + currency_gain
		state_table.villages[location_table.village_name].reputation = state_table.villages[location_table.village_name].reputation + 1
		return state_table
	end)
end




function create_quest_unlock_action(action_name, message , quest_budget, quest_flag_name, location_table)
	return create_action(
		action_name,
		function(state_table)
			return 
			{
				isEqual(state_table.current_agent_table.location, location_table),
				state_table.current_agent_table.quest_budget >= quest_budget,
				not get_table_flag(state_table.current_agent_table.quests_in_progress, quest_flag_name),
				not get_table_flag(state_table.current_agent_table.quests_completed, quest_flag_name)
			}
		end,
		function(state_table)
			state_table.current_agent_table.quest_budget = state_table.current_agent_table.quest_budget - quest_budget
			state_table.current_agent_table.quests_in_progress[quest_flag_name] = true
			state_table.message = message
			return state_table
	end)
end

function create_action_selection_string(action_state_table)
	local message_to_concat = {}
	-- append action names 
	for i, action_state_pair in ipairs(action_state_table) do
		message_to_concat[#message_to_concat+1] = tostring(i) .. ": Player ".. action_state_pair.action.name
	end
	return table.concat(message_to_concat, "\n")
end
