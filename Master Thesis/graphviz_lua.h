#pragma once

#include <boost/graph/graphviz.hpp>

// TODO: low priority: subgraphs:
// https://stackoverflow.com/questions/29312444/how-to-write-graphviz-subgraphs-with-boostwrite-graphviz/29336351


class graphviz_lua
{
	struct vertex_props
	{
		std::string name;
	};

	struct edge_props
	{
		std::string name;
	};
	using graph = boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, vertex_props, edge_props>;

public:
	graphviz_lua() = default;

	void write_graph(const std::string & filename)
	{
		std::ofstream label_writer_example_dot(filename);

		write_graphviz(label_writer_example_dot, graph_,
			make_label_writer(get(&vertex_props::name, graph_)),
			make_label_writer(get(&edge_props::name, graph_)));
	}

	auto add_vertex_to_graph(const std::string & vertex_label)
	{
		return add_vertex({ vertex_label }, graph_);
	}



	auto add_edge_to_graph(const std::string & edge_label, const std::string & vertex_a_label, const std::string & vertex_b_label)
	{
		// based on: https://stackoverflow.com/questions/45925625/accessing-specific-vertices-in-boostgraph
		const auto find_vertex_by_label = [&](const std::string & vertex_label) 
		{
			for (auto vd : boost::make_iterator_range(vertices(graph_)))
			{
				if (vertex_label == graph_[vd].name)
					return vd;
			}
			// add vertex if not found
			return add_vertex_to_graph(vertex_label);
		};

		return add_edge(find_vertex_by_label(vertex_a_label), find_vertex_by_label(vertex_b_label), { edge_label }, graph_);
	}

private:
	graph graph_;
};