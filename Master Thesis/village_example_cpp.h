#pragma once
#include <iostream>

#include "custom_json.h"
#include "rng.h"


// easy post-increment and post-decrement operators for convenience
// should lead to errors when used with wrong types

inline custom_json::value_type operator++(custom_json::value_type& v, int)
{
	return v.get_ref<custom_json::number_unsigned_t &>()++;
}

inline custom_json::value_type operator--(custom_json::value_type& v, int)
{
	return v.get_ref<custom_json::number_unsigned_t &>()--;
}

inline void village_test()
{
	custom_json default_villager;
	read_json_from_file(default_villager, "villager default.json");

	custom_json worldstate;
	read_json_from_file(worldstate, "worldstate default.json");

	const size_t start_food = 5;
	worldstate["village"]["food"] = start_food;

	const size_t num_villagers = 10;
	for (size_t i = 0; i < num_villagers; i++)
	{
		// modify default 'villager'
		custom_json new_villager = default_villager;
		new_villager["name"] = rng::random_hiragana_name();
		new_villager["hunger"] = rng::random_unsigned(0, 2);

		worldstate["village"]["villagers"] += new_villager;
	}


	write_json_to_file(worldstate, "worldstate day0.json");

	const size_t days_of_simulation = 30;

	auto & villagers_ref = worldstate["village"]["villagers"].get_ref<custom_json::array_t &>();


	for (size_t i = 0; i < days_of_simulation; i++)
	{
		// sort villagers by hunger
		std::sort(std::begin(villagers_ref), std::end(villagers_ref),
			[](custom_json & a, custom_json & b)
		{
			return a["hunger"] < b["hunger"];
		});

		for (auto & villager : villagers_ref)
		{
			if (villager["hunger"] > 0)
			{
				if (worldstate["village"]["food"] > 0)
				{
					// eats food from village storage
					worldstate["village"]["food"]--;
					villager["hunger"]--;

				}
				else
				{
					// no food available, search 
					// randomness as placeholder, can be replaced with GOAP
					if (rng::random_bool(0.25))
					{
						// found food elsewhere
						villager["hunger"]--;

						if (rng::random_bool(0.75))
						{
							// found enough to store leftovers
							std::cout << "Villager " << villager["name"] << " found additional food \n";
							worldstate["village"]["food"]++;
						}
					}
					else
					{
						// did not find food, increase hunger
						villager["hunger"]++;
					}
				}
			}
		}

		// remove villagers who where starving for 3 days
		villagers_ref.erase(std::remove_if(std::begin(villagers_ref), std::end(villagers_ref),
			[](custom_json & v)
		{
			const bool starvation = v["hunger"] >= 3;
			if (starvation)
				std::cout << "Villager " << v["name"] << " died of starvation \n";
			return starvation;
		}),
			std::end(villagers_ref));

		// day over
		worldstate["day"]++;
	}

	write_json_to_file(worldstate, "worldstate end.json");

	std::cout << "village_test finished \n";
}