#pragma once

#include <vector>
#include <algorithm>

#include "action_lua.h"
#include "world_state_lua.h"

class action_planner_lua
{
public:
	action_planner_lua() = default;
	action_planner_lua(const action_planner_lua &) = default;

	void add_action_array(const sol::table & action_array)
	{
		action_array.for_each([&](const sol::object& /*key*/, const sol::object& lua_value)
		{
			const sol::table action_table = lua_value.as<sol::table>();
			add_action(action_table);
		});
	}

	bool add_action(const sol::table & action_as_table)
	{
		return add_action(action_lua(action_as_table));
	}

	bool add_action(const action_lua & action)
	{
		if (action_exists(action))
			return false;

		actions_.emplace_back(action);

		return true;
	}

	bool add_action(action_lua&& action)
	{
		if (action_exists(action))
			return false;

		actions_.emplace_back(action);

		return true;
	}

	


	std::vector<std::pair<action_lua, world_state_lua>> possible_actions_and_states(const world_state_lua& from) const
	{
		std::vector<std::pair<action_lua, world_state_lua>> result;
		for (const action_lua& action : actions_)
		{
			std::optional<world_state_lua> opt_state = from.create_new_state_from_action(action);
			if (opt_state)
				result.emplace_back(std::make_pair(action, *opt_state));
		}
		return result;
	}

	// version is callable from lua
	// returns array of {action, state} tables
	sol::table possible_actions_and_states_table(const sol::table & from_world_state_table, sol::this_state lua_state) const
	{
		sol::state_view lua = lua_state;

		const world_state_lua from(from_world_state_table);
		auto result_cpp = possible_actions_and_states(from);
		sol::table result_lua = lua.create_table(result_cpp.size());
		for (const auto[action, state] : result_cpp)
		{
			result_lua.add(lua.create_table_with(
				"action", action.table(),
				"state", state.table()
			));
		}
		return result_lua;
	}


private:
	std::vector<action_lua> actions_;

	bool action_exists(const action_lua& action) const
	{
		return std::find(std::cbegin(actions_), std::cend(actions_), action) != std::cend(actions_);
	}

	
};
