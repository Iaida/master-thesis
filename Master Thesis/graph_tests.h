#pragma once
#include <charconv>

#include <boost/graph/graphviz.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/undirected_graph.hpp>
#include "enumerate.h"


//
//void graph_test()
//{
//	using namespace boost;
//
//	// Vertex properties
//	typedef property<vertex_name_t, std::string, property<vertex_color_t, float>> vertex_p;
//	// Edge properties
//	typedef property<edge_weight_t, double> edge_p;
//	// Graph properties
//	typedef property<graph_name_t, std::string> graph_p;
//	// adjacency_list-based type
//	typedef adjacency_list<vecS, vecS, directedS, vertex_p, edge_p, graph_p> graph_t;
//
//	// Construct an empty graph and prepare the dynamic_property_maps.
//	graph_t graph(0);
//	dynamic_properties dp;
//
//	auto name = get(vertex_name, graph);
//	dp.property("node_id", name);
//
//	auto mass = get(vertex_color, graph);
//	dp.property("mass", mass);
//
//	auto weight = get(edge_weight, graph);
//	dp.property("weight", weight);
//
//	// Use ref_property_map to turn a graph property into a property map
//	ref_property_map<graph_t*, std::string> gname(get_property(graph, graph_name));
//	dp.property("name", gname);
//
//	// Sample graph as an std::istream;
//	std::istringstream gvgraph("digraph { graph [name=\"graphname\"]  a  c e [mass = 6.66] }");
//
//	bool status = read_graphviz(gvgraph, graph, dp, "node_id");
//	std::cout << "read_graphviz: " << status << '\n';
//}


//void village_map_test()
//{
//	using namespace boost;
//
//	// Vertex properties
//	typedef property<vertex_name_t, std::string> vertex_p;
//	// Edge properties
//	typedef property<edge_weight_t, double> edge_p;
//	// Graph properties
//	typedef property<graph_name_t, std::string> graph_p;
//	// adjacency_list-based type
//	typedef adjacency_list<vecS, vecS, undirectedS, vertex_p, edge_p, graph_p> graph_t;
//
//	// Construct an empty graph and prepare the dynamic_property_maps.
//	graph_t graph(0);
//	dynamic_properties dp;
//
//	auto name = get(vertex_name, graph);
//	dp.property("node_id", name);
//
//	auto mass = get(vertex_color, graph);
//	dp.property("mass", mass);
//
//	auto weight = get(edge_weight, graph);
//	dp.property("weight", weight);
//
//	// Use ref_property_map to turn a graph property into a property map
//	ref_property_map<graph_t*, std::string> gname(get_property(graph, graph_name));
//	dp.property("name", gname);
//
//
//
//
//	/*std::ofstream graph_file_dp("GraphFileDP.gv");
//	write_graphviz_dp(graph_file_dp, graph, dp, std::string("id"));*/
//
//
//	std::ofstream graph_file("GraphFile.gv");
//	write_graphviz(graph_file, graph);
//}


// https://www.boost.org/doc/libs/1_68_0/libs/graph/example/undirected_graph.cpp
//
//void undirected_graph_test()
//{
//	using namespace boost;
//
//	// Vertex properties
//	typedef property<vertex_name_t, std::string> vertex_p;
//	// Edge properties
//	typedef property<edge_weight_t, double> edge_p;
//	// Graph properties
//	typedef property<graph_name_t, std::string> graph_p;
//
//	typedef undirected_graph<vertex_p, edge_p, graph_p> Graph;
//
//	// Create a graph object
//	Graph g;
//
//	// Add vertices
//	graph_traits<Graph>::vertex_descriptor v0 = g.add_vertex();
//	
//	graph_traits<Graph>::vertex_descriptor v1 = g.add_vertex();
//	graph_traits<Graph>::vertex_descriptor v2 = g.add_vertex();
//
//	// Add edges
//	g.add_edge(v0, v1);
//	g.add_edge(v1, v2);
//}


// based on: https://www.boost.org/doc/libs/1_68_0/libs/graph/doc/write-graphviz.html
inline void write_graphviz_example()
{
	string_id dax_h("dax.h"),
	          yow_h("yow.h"),
	          boz_h("boz.h"),
	          zow_h("zow.h"),
	          foo_cpp("foo.cpp"),
	          foo_o("foo.o"),
	          bar_cpp("bar.cpp"),
	          bar_o("bar.o"),
	          libfoobar_a("libfoobar.a"),
	          zig_cpp("zig.cpp"),
	          zig_o("zig.o"),
	          zag_cpp("zag.cpp"),
	          zag_o("zag.o"),
	          libzigzag_a("libzigzag.a"),
	          killerapp("killerapp");

	std::vector<string_id> nodes =
	{
		dax_h,
		yow_h,
		boz_h,
		zow_h,
		foo_cpp,
		foo_o,
		bar_cpp,
		bar_o,
		libfoobar_a,
		zig_cpp,
		zig_o,
		zag_cpp,
		zag_o,
		libzigzag_a,
		killerapp
	};

	typedef std::pair<size_t, size_t> edge;
	std::vector<edge> used_by =
	{
		edge(dax_h, foo_cpp), edge(dax_h, bar_cpp), edge(dax_h, yow_h),
		edge(yow_h, bar_cpp), edge(yow_h, zag_cpp),
		edge(boz_h, bar_cpp), edge(boz_h, zig_cpp), edge(boz_h, zag_cpp),
		edge(zow_h, foo_cpp),
		edge(foo_cpp, foo_o),
		edge(foo_o, libfoobar_a),
		edge(bar_cpp, bar_o),
		edge(bar_o, libfoobar_a),
		edge(libfoobar_a, libzigzag_a),
		edge(zig_cpp, zig_o),
		edge(zig_o, libzigzag_a),
		edge(zag_cpp, zag_o),
		edge(zag_o, libzigzag_a),
		edge(libzigzag_a, killerapp)
	};

	const size_t number_edges = used_by.size();
	std::vector<int> weights(number_edges);
	std::fill(weights.begin(), weights.end(), 1);

	using namespace boost;

	typedef property<vertex_color_t, default_color_type> vertex_p;
	typedef property<edge_weight_t, int> edge_p;

	typedef adjacency_list<vecS, vecS, directedS, vertex_p, edge_p> graph;
	const graph g(used_by.begin(), used_by.end(), weights.begin(), nodes.size());

	std::ofstream write_graphviz_example_dot("write_graphviz_example.gv");

	std::vector<std::string> vertex_labels(nodes.size());
	std::transform(nodes.begin(), nodes.end(), vertex_labels.begin(),
		[](string_id string_id) { return string_id.to_string(); });
	const auto vertex_writer = make_label_writer(vertex_labels.data());

	/*std::vector<std::string> edge_labels(weights.size());
	std::transform(weights.begin(), weights.end(), edge_labels.begin(),
		[](int weight) { return std::to_string(weight); });
	const auto edge_writer = make_label_writer(edge_labels.data());*/


	write_graphviz(write_graphviz_example_dot, g, vertex_writer);
}


// based on: https://stackoverflow.com/questions/51138794/how-to-use-boost-make-label-writer-to-write-edge-properties
inline void label_writer_example()
{
	using namespace boost;

	struct vertex_props
	{
		std::string name;
	};

	struct edge_props
	{
		std::string name;
	};
	
	adjacency_list<vecS, vecS, undirectedS, vertex_props, edge_props> graph;
	const auto v1 = add_vertex({ "one" }, graph);
	const auto v2 = add_vertex({ "two" }, graph);
	const auto v3 = add_vertex({ "three" }, graph);


	add_edge(v2, v1, { "e1" }, graph);
	add_edge(v3, v2, { "e2" }, graph);
	add_edge(v2, v3, { "e3" }, graph);
	add_edge(v3, v1, { "e4" }, graph);

	std::ofstream label_writer_example_dot("label_writer_example.gv");

	write_graphviz(label_writer_example_dot, graph,
		make_label_writer(get(&vertex_props::name, graph)),
		make_label_writer(get(&edge_props::name, graph)));
}

// based on syntax of: https://stackoverflow.com/questions/49007034/print-or-iterate-over-the-vertices-with-vertex-descriptor-boost-graph

inline void static_graph_example()
{
	using namespace boost;

	struct vertex_props
	{
		std::string name;
	};

	struct edge_props
	{
		std::string name;
	};

	using custom_graph = adjacency_list<vecS, vecS, undirectedS, vertex_props, edge_props>;

	custom_graph graph;
	
	const custom_graph::vertex_descriptor market = add_vertex({ "Market" }, graph);
	const custom_graph::vertex_descriptor church = add_vertex({ "Church" }, graph);
	const custom_graph::vertex_descriptor forge = add_vertex({ "Forge" }, graph);
	const custom_graph::vertex_descriptor fields = add_vertex({ "Fields" }, graph);
	const custom_graph::vertex_descriptor farm = add_vertex({ "Farm" }, graph);
	const custom_graph::vertex_descriptor mill = add_vertex({ "Mill" }, graph);
	const custom_graph::vertex_descriptor forest = add_vertex({ "Forest" }, graph);
	const custom_graph::vertex_descriptor castle = add_vertex({ "Castle" }, graph);
	
	add_edge(market, church, { "e1" }, graph);
	add_edge(market, forge, { "e2" }, graph);
	add_edge(market, fields, { "e3" }, graph);
	add_edge(fields, farm, { "e4" }, graph);
	add_edge(fields, mill, { "e5" }, graph);
	add_edge(fields, forest, { "e6" }, graph);
	add_edge(forest, castle, { "e7" }, graph);

	std::ofstream own_example_dot("own_example.gv");

	write_graphviz(own_example_dot, graph,
		make_label_writer(get(&vertex_props::name, graph)),
		make_label_writer(get(&edge_props::name, graph)));

	const auto start_vertex = market;

	auto current_vertex = start_vertex;

	while (true)
	{
		auto neighbor_edges = make_iterator_range(out_edges(current_vertex, graph));
		const size_t number_edges = neighbor_edges.size();

		std::cout << "Current vertex <" << graph[current_vertex].name << "> has " <<
			number_edges << " adjacent edges: \n";

		// mapping of index -> edge for easier choice input with a vector of edges
		std::vector<decltype(neighbor_edges)::reference> edge_vector(number_edges);
		size_t counter = 0;
		
		for (auto neighbor_edge : neighbor_edges)
		{
			edge_vector[counter] = neighbor_edge;
			const auto neighbor_vertex_target = target(neighbor_edge, graph);
			std::cout << counter << ": edge <" << graph[neighbor_edge].name << "> has target vertex <" <<
				graph[neighbor_vertex_target].name << "> \n";
			counter++;
		}

		size_t input;
		do
		{
			std::cout << "Choose an edge by typing its edge index: \n";
			std::cin >> input;
		}
		while (input >= edge_vector.size());

		current_vertex = target(edge_vector[input], graph);
	}
}


inline void dynamic_graph_example()
{
	using namespace boost;

	struct vertex_props
	{
		string_id name;
	};

	struct edge_props
	{
		string_id name;
	};

	
	using custom_graph = adjacency_list<setS, setS, undirectedS, vertex_props, edge_props>;
	custom_graph graph;

	struct transition
	{
		string_id name;
		string_id source;
		string_id target;
	};

	const std::vector<transition> transitions =
	{
		{"e1", "Market", "Church"},
		{"e2", "Market", "Forge"},
		{"e3", "Market", "Fields"},
		{"e4", "Fields", "Farm"},
		{"e5", "Fields", "Mill"},
		{"e6", "Fields", "Forest"},
		{"e7", "Forest", "Castle"}
	};

	const vertex_props start_vertex_props = { "Market" };

	const custom_graph::vertex_descriptor start_vertex = add_vertex(start_vertex_props, graph);

	auto current_vertex = start_vertex;

	bool running = true;
	while (running)
	{
		vertex_props current_vertex_properties = graph[current_vertex];
		auto neighbor_edges = make_iterator_range(out_edges(current_vertex, graph));


		// add possible edges
		std::for_each(std::cbegin(transitions), std::cend(transitions),
		              [&](const transition& t)
		              {
			              if (t.source == current_vertex_properties.name)
			              {
				              // check if transition edge not already present
				              if (std::none_of(std::cbegin(neighbor_edges), std::cend(neighbor_edges),
				                               [&](auto edge) { return graph[edge].name == t.name; }))
				              {
					              const auto target_vertex = add_vertex({t.target}, graph);
					              const auto add_edge_result = add_edge(current_vertex, target_vertex, {t.name}, graph);
					              std::cout << "add_edge_result: " << add_edge_result.second << '\n';
				              }
			              }
		              });

		// update 'neighbor_edges' with new edges
		neighbor_edges = make_iterator_range(out_edges(current_vertex, graph));

		// choose edge
		std::cout << "Current vertex <" << current_vertex_properties.name << "> has the following outgoing edges: \n";

		
		// mapping of index -> edge for easier input of a choice with a vector of edges

		std::vector<decltype(neighbor_edges)::reference> edge_vector;
		size_t counter = 0;
		for (auto current_neighbor_edge : neighbor_edges)
		{
			edge_vector.emplace_back(current_neighbor_edge);
			const auto neighbor_vertex_target = target(current_neighbor_edge, graph);
			std::cout << counter << ": edge <" << graph[current_neighbor_edge].name << "> has target vertex <" <<
				graph[neighbor_vertex_target].name << "> \n";
			counter++;
		}

		
		bool input_valid = false;
		while (!input_valid && running)
		{
			std::cout << "Choose an edge by typing its edge index or type 'q' to quit: \n";
			std::string raw_input;
			std::cin >> raw_input;
			size_t edge_index_input;
			const auto parse_result = std::from_chars(raw_input.c_str(),
				raw_input.c_str() + raw_input.size(),
				edge_index_input);
			if (parse_result.ec == std::errc::invalid_argument)
			{
				if (raw_input == "q")
					running = false;
				else
					std::cout << "Input was not a valid index \n";
			}
			else if (edge_index_input >= edge_vector.size())
			{
				std::cout << "Chosen index is too high \n";
			}
			else
			{
				input_valid = true;
				current_vertex = target(edge_vector[edge_index_input], graph);
			}
		}		
	}

	std::ofstream dynamic_graph_example_dot("dynamic_graph_example.gv");

	// https://stackoverflow.com/questions/45426129/boostwrite-graphviz-how-to-generate-the-graph-horizontally
	dynamic_properties dp;
	dp.property("node_id", get(&vertex_props::name, graph));
	dp.property("label", get(&vertex_props::name, graph));
	dp.property("label", get(&edge_props::name, graph));
	dp.property("rankdir", boost::make_constant_property<custom_graph*>(std::string("LR")));

	write_graphviz_dp(dynamic_graph_example_dot, graph, dp);
}