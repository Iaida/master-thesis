print("table_function_parameter_test.lua start")

LuaUtilities = require "LuaUtilities"

--check assignment of values to same keys
function table_return(bla, blub)
	return
	{
		bla = bla,
		blub = blub
	}
end

print(table.tostring(table_return(1, 2)))


print("table_function_parameter_test.lua end")