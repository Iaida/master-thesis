#pragma once
#include <string>
#include <array>
#include <vector>

#include "action_simple.h"
#include "enumerate.h"


class tic_tac_toe
{
public:
	static const size_t edge_length = 3;
	enum class field_value { free, cross, circle };

	
	// vector of goals which need multiple atoms to be true
	static std::vector<std::vector<std::string>> goals(const field_value target_value)
	{
		return
		{
			// rows
			{
				atom_from_num_pad_and_field_value(1, target_value),
				atom_from_num_pad_and_field_value(2, target_value),
				atom_from_num_pad_and_field_value(3, target_value),
			},
			{
				atom_from_num_pad_and_field_value(4, target_value),
				atom_from_num_pad_and_field_value(5, target_value),
				atom_from_num_pad_and_field_value(6, target_value),
			},
			{
				atom_from_num_pad_and_field_value(7, target_value),
				atom_from_num_pad_and_field_value(8, target_value),
				atom_from_num_pad_and_field_value(9, target_value),
			},
			// columns
			{
				atom_from_num_pad_and_field_value(1, target_value),
				atom_from_num_pad_and_field_value(4, target_value),
				atom_from_num_pad_and_field_value(7, target_value),
			},
			{
				atom_from_num_pad_and_field_value(2, target_value),
				atom_from_num_pad_and_field_value(5, target_value),
				atom_from_num_pad_and_field_value(8, target_value),
			},
			{
				atom_from_num_pad_and_field_value(3, target_value),
				atom_from_num_pad_and_field_value(6, target_value),
				atom_from_num_pad_and_field_value(9, target_value),
			},
			// diagonal
			{
				atom_from_num_pad_and_field_value(3, target_value),
				atom_from_num_pad_and_field_value(5, target_value),
				atom_from_num_pad_and_field_value(7, target_value),
			},
			{
				atom_from_num_pad_and_field_value(1, target_value),
				atom_from_num_pad_and_field_value(5, target_value),
				atom_from_num_pad_and_field_value(9, target_value),
			}
		};
	}

	static std::vector<std::string> start_atoms()
	{
		return tic_tac_toe().to_atoms();
	}

	static std::vector<action_simple> possible_actions(const field_value target_value)
	{
		std::vector<action_simple> results;
		for (auto [i, empty_atom] : enumerate(tic_tac_toe().to_atoms()))
		{
			std::string modified_atom = empty_atom;
			modified_atom[1] = field_value_to_char(target_value);
			std::string name = modified_atom;
			const float cost = cost_from_num_pad(i + 1);

			action_simple action(name, cost,
				{ { empty_atom, true } },
				{ { empty_atom, false }, { modified_atom, true } });
			results.emplace_back(action);
		}
		return results;
	}

private:

	tic_tac_toe()
	{
		field_.fill({ {field_value::free, field_value::free, field_value::free} });
	}

	static char field_value_to_char(const field_value field_value)
	{
		switch (field_value)
		{
		case field_value::free:
			return '-';
		case field_value::cross:
			return 'X';
		case field_value::circle:
			return 'O';
		default:
			return 0;
		}
	}

	std::vector<std::string> to_atoms() const
	{
		std::vector<std::string> results;
		for (size_t i = 1; i < 10; i++)
		{
			results.emplace_back(atom_from_num_pad(i));
		}
		return results;
	}

	field_value field_value_from_num_pad(const size_t num_pad_number) const
	{
		if (num_pad_number < 1 || num_pad_number > 9)
			throw std::out_of_range("num_pad_number should only be between 1 and 9");

		const size_t one_dim_index = num_pad_number - 1;
		const size_t row = one_dim_index % 3;
		const size_t column = one_dim_index / 3;
		return field_[column][row];
	}

	static std::string atom_from_num_pad_and_field_value(const size_t num_pad_number, const field_value field_value)
	{
		const char c = field_value_to_char(field_value);
		const std::string num_pad_string = std::to_string(num_pad_number);
		return num_pad_string + c;
	}

	std::string atom_from_num_pad(const size_t num_pad_number) const
	{
		return atom_from_num_pad_and_field_value(num_pad_number, field_value_from_num_pad(num_pad_number));
	}

	static float cost_from_num_pad(const size_t num_pad_number)
	{
		// middle part of 4 goals
		// corners part of 3 goals
		// sides part of 2 goals
		switch (num_pad_number)
		{
		case 2:
		case 4:
		case 6:
		case 8:
			return 1.0f / 2.0f;
		case 1:
		case 3:
		case 7:
		case 9:
			return 1.0f / 3.0f;
		case 5:
			return 1.0f / 4.0f;
		default:
			throw std::out_of_range("num_pad_number should only be between 1 and 9");
		}
	}

	std::array<std::array<field_value, edge_length>, edge_length> field_{};
};



