#pragma once

#include <optional>
#include <algorithm>

#include "action_lua.h"
#include "callable_lua_functions.h"

class world_state_lua
{
public:
	world_state_lua() : lua_state_view(state_ptr)
	{
		table_ = lua_state_view.create_table();
	}

	world_state_lua(const sol::table & table) : lua_state_view(state_ptr)
	{
		table_ = table;
	}

	std::optional<world_state_lua> create_new_state_from_action(const action_lua& action) const
	{
		if (!all_conditions_achieved(action.preconditions()))
			return std::nullopt;

		return std::optional<world_state_lua>(apply_effects_to_table(action.effects()));
	}

	std::string to_string() const
	{
		return callable_lua_functions::table_to_string(lua_state_view, table_);
	}

	

	// returns the number of atoms which are mismatched or not present
	size_t distance_to_goal(const sol::function & goal_conditions) const
	{
		return number_mismatched_conditions(goal_conditions);
	}

	bool all_conditions_achieved(const sol::function & preconditions) const
	{
		return number_mismatched_conditions(preconditions) == 0;
	}

	bool any_goal_reached(const std::vector<sol::function> & multiple_goal_conditions) const
	{
		return std::any_of(std::cbegin(multiple_goal_conditions), std::cend(multiple_goal_conditions),
			[&](const sol::function& goal_conditions) -> bool
		{
			return all_conditions_achieved(goal_conditions);
		});
	}

	// TODO : const ref? -> check
	sol::table table() const
	{
		return table_;
	}

	bool operator==(const world_state_lua& rhs) const
	{
		return callable_lua_functions::table_equality(lua_state_view, table_, rhs.table_);
	}

	

	friend std::ostream& operator<<(std::ostream& out, const world_state_lua& ws)
	{
		return out << ws.to_string();
	}

	// implementation needed for boost::graph properties
	friend std::istream& operator>>(std::istream& in, world_state_lua& ws)
	{
		std::string string;
		auto & result = in >> string;
		ws = world_state_lua();
		return result;
	}

	static lua_State * state_ptr;

private:

	sol::table table_;
	sol::state_view lua_state_view;

	sol::table apply_effects_to_table(const sol::function & effects) const
	{
		// logical errors with cyoa example on restart when not using returns and just manipulating passed table in lua

		// using return in lua function and deepcopy in c++, allows to skip deepcopy in lua effects
		sol::protected_function_result result = effects(callable_lua_functions::table_deepcopy(lua_state_view, table_));
		//sol::protected_function_result result = effects(table_);

		if (!result.valid())
		{
			sol::error err = result;
			std::cout << "effect call failed: " << err.what() << '\n';
			throw std::exception("effect call failed");
		}
		if (result.get_type() != sol::type::table)
		{
			std::cout << "effect function did not return a table \n";
			throw std::exception("effect function did not return a table");
		}

		return result.get<sol::table>();
	}

	size_t number_mismatched_conditions(const sol::function & conditions_function) const
	{		
		sol::protected_function_result result = conditions_function(table_);
		if (!result.valid())
		{
			sol::error err = result;
			std::cout << "conditions call failed: " << err.what() << '\n';
			throw std::exception("conditions call failed");
		}

		// 'conditions_function' returns array of boolean values indicating match or mismatch
		sol::table bool_array = result;

		size_t number_mismatched = 0;
		bool_array.for_each([&number_mismatched](const sol::object& /*key*/, const sol::object& value)
		{
			if (!value.as<bool>())
				number_mismatched++;
		});
		return number_mismatched;
	}
};

lua_State * world_state_lua::state_ptr = nullptr;

