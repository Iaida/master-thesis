print("market_crier.lua start")

LuaUtilities = require "LuaUtilities"

function addNewsToMessage(base_message, news)
	local message_to_concat = {base_message}
	message_to_concat[#message_to_concat+1] = " A market crier exclaims: '"
	message_to_concat[#message_to_concat+1] = news
	message_to_concat[#message_to_concat+1] = "'"
	return table.concat(message_to_concat)
end

function addReputationToLocations(locations, current_location, reputation_value)
	for _, loc in pairs(locations) do
		if loc == current_location then
			loc.reputation = loc.reputation + reputation_value
		else
			-- replace with proper function for distance calculation
			local distance_factor = 0.5
			loc.reputation = loc.reputation + distance_factor * reputation_value
		end
	end
end

local starting_world_state = 
{
	villages = 
	{
		village_a = 
		{
			reputation = 0
		},
		village_b = 
		{
			reputation = 0
		}
	},

	message = addNewsToMessage("You are in the marketplace.", "A dragon is still rampaging in our fields and taking our livestock!"),
	-- name added to make it more personal
	name = "Siegfried"
}

local ap = action_planner.new(
{
	{
		name = "Go and slay the dragon",
		cost = 1.0,
		preconditions = function(state_table)
			return { not get_table_flag(state_table, "dragon_slain")}
		end,
		effect = function(state_table)
			local result = table.deepcopy(state_table)
			result.dragon_slain = true
			addReputationToLocations(result.villages, result.villages.village_a, 10)
	
			local news = "The dragon has been slain by a wandering adventurer by the name of " .. result.name .. "!"
			result.message = addNewsToMessage("You are in the marketplace.", news)
			return result
		end
	}
})



local quitting = false

local current_state = starting_world_state

repeat
	local ast = ap:possible_actions_and_states_table(current_state)

	-- append action names to 'current_state.message'
	local message_to_concat = {current_state.message}
	for i, action_state_pair in ipairs(ast) do
		message_to_concat[#message_to_concat+1] = tostring(i) .. ": ".. action_state_pair.action.name
	end
	local message = table.concat(message_to_concat, "\n")

	local valid_input = index_strings(#ast)
	-- allow quitting as valid input
	valid_input[#valid_input+1] = "q"

	-- print and read user input
	local input = read_and_return_valid_input(message, valid_input)
	if (input == "q") then 
		print("q input")
		quitting = true
		do break end
	end

	-- use input to choose action and assign new state
	local chosen_index = tonumber(input)
	current_state = ast[chosen_index].state

	

until quitting

print("quitting...")

print("last world state for a view into the reputation values: ")
print(table.tostring(current_state))


print("market_crier.lua end")